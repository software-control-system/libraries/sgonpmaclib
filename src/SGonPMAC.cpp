//=============================================================================
// SGonPMAC.cpp
//=============================================================================
// abstraction.......SMARGON PMAC Application Programming Interface
// class.............SMARGON library API implementation
// original author...S. MINOLLI
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "SGonPMAC/SGonPMAC.h"
#include <locale> // tolower
#include <algorithm> // transform

// special windows:
#if (defined WIN32 || defined _WIN32)
#include <windows.h>
#endif

#include <yat/utils/XString.h>

namespace sgonpmaclib {

#if !defined (_SIMULATION_)
// ============================================================================
//- Check controller pointer:
#define CHECK_PMAC_CTRL \
  if (!m_sshdriver) \
  { \
     err_code = pmac_internal_ERROR; \
     m_error_string = "Internal error: controller pointer is NULL! "; \
     return err_code; \
  }

//- Compose PPMAC CS variable:
#define PPMAC_CS_VAR(VAR, CS)     \
  std::string axis_ref = kPMAC_VAR_CS \
      + CS + kPMAC_VAR_SEPARATOR + VAR;

//- Compose PPMAC Axis variable:
#define PPMAC_AXIS_VAR(VAR, INDEX)     \
  std::string axis_ref = kPMAC_VAR_AXIS \
      + INDEX + kPMAC_VAR_SEPARATOR + VAR;

// ============================================================================
#endif

// ============================================================================
// SGonPMAC::SGonPMAC
// ============================================================================
SGonPMAC::SGonPMAC()
{
  this->m_error_string = "";
  m_is_connected = false;

#if !defined (_SIMULATION_)
  this->m_sshdriver = NULL;
  this->m_axis_list.clear();
#else
  m_sim_mp_state = false;
  
  m_sim_box_status.WDTFault0 = 0;
  m_sim_box_status.WDTFault0 = 0;
  m_sim_box_status.PwrOnFault = 0;
  m_sim_box_status.ProjectLoadErr = 0 ;
  m_sim_box_status.ConfigLoadErr = 0;
  m_sim_box_status.HWChangeErr = 0;
  m_sim_box_status.FileConfigErr = 0;
  m_sim_box_status.Default = 0;
  m_sim_box_status.NoClocks = 0;
  m_sim_box_status.AbortAll = 0;
  m_sim_box_status.BusOverVoltage = 0;
  m_sim_box_status.OverTemp = 0;
  m_sim_box_status.BusUnderVoltage = 0;

  m_sim_box_status.NotUsed10 = 0;
  m_sim_box_status.NotUsed11 = 0;
  m_sim_box_status.NotUsed12 = 0;
  m_sim_box_status.FwdKinErr = 0;
  m_sim_box_status.InvKinErr = 0;
  m_sim_box_status.CalcCompErr = 0;

  m_sim_box_status.NotUsed19 = 0;
  m_sim_box_status.NotUsed20 = 0;
  m_sim_box_status.NotUsed21 = 0;
  m_sim_box_status.NotUsed22 = 0;
  m_sim_box_status.NotUsed23 = 0;
  m_sim_box_status.NotUsed24 = 0;
  m_sim_box_status.NotUsed25 = 0;
  m_sim_box_status.NotUsed26 = 0;
  m_sim_box_status.NotUsed27 = 0;
  m_sim_box_status.NotUsed28 = 0;
  m_sim_box_status.NotUsed29 = 0;
  m_sim_box_status.NotUsed30 = 0;
  m_sim_box_status.NotUsed31 = 0;
#endif

}

// ============================================================================
// SGonPMAC::~SGonPMAC
// ============================================================================
SGonPMAC::~SGonPMAC ()
{
#if !defined (_SIMULATION_)
  this->m_axis_list.clear();

  if (this->m_sshdriver)
  {
    // disconnect if connected
    if (m_is_connected)
    {
      this->m_sshdriver->disconnectSSH();
    }

    // delete pointer
    delete this->m_sshdriver;
    this->m_sshdriver = NULL;
  }
#endif
}

// ============================================================================
// SGonPMAC::get_error_string
// ============================================================================
std::string SGonPMAC::get_error_string()
{
  std::string err_str = this->m_error_string;
  this->m_error_string = "";
  return err_str;
}

// ============================================================================
// SGonPMAC::init_api
// ============================================================================
E_pmac_errno SGonPMAC::init_api(std::string ipAddress, std::string user, 
                                 std::string passwd, unsigned int tcpPort)
{
  std::cout << "SGonPMAC::init_api() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)

  SSHDriverStatus ssh_status;

  // create new controller if none with IP address
  if (!this->m_sshdriver)
  {
    this->m_sshdriver = new SSHDriver(ipAddress.c_str());
  
    CHECK_PMAC_CTRL;
    std::cout << "New controller created" << std::endl;
  }
  else
  {
    // if controller already created, check if connection closed
    if (this->m_is_connected)
    {
      // cannot resuse controller, because already opened!
      err_code = pmac_internal_ERROR;
      this->m_error_string = "Cannot open connection: already opened! Close it first.";
      return err_code;
    }
  }

  // set connection parameters:
  //- TCP PORT
  char port_c[10];
  sprintf(port_c, "%d", tcpPort);
  ssh_status = m_sshdriver->setPort(port_c);
  if (ssh_status != SSHDriverSuccess)
  {
    // error
    err_code = pmac_libssh_ERROR;
    char errno_c[10];
    sprintf(errno_c, "%d", ssh_status);

    this->m_error_string = "Lib ssh error occurred while setting tcp port: " + std::string(errno_c);
    return err_code;
  }

  //- User name
  ssh_status = m_sshdriver->setUsername(user.c_str());
  if (ssh_status != SSHDriverSuccess)
  {
    // error
    err_code = pmac_libssh_ERROR;
    char errno_c[10];
    sprintf(errno_c, "%d", ssh_status);

    this->m_error_string = "Lib ssh error occurred while setting username: " + std::string(errno_c);
    return err_code;
  }

  //- password
  ssh_status = m_sshdriver->setPassword(passwd.c_str());
  if (ssh_status != SSHDriverSuccess)
  {
    // error
    err_code = pmac_libssh_ERROR;
    char errno_c[10];
    sprintf(errno_c, "%d", ssh_status);

    this->m_error_string = "Lib ssh error occurred while setting passwd: " + std::string(errno_c);
    return err_code;
  }

  std::cout << "try to connect..." << std::endl;
  ssh_status = m_sshdriver->connectSSH();
  if (ssh_status != SSHDriverSuccess)
  {
    // error
    std::cout << "Connection not open! ssh status = " << ssh_status << std::endl;
    err_code = pmac_libssh_ERROR;
    char errno_c[10];
    sprintf(errno_c, "%d", ssh_status);

    this->m_error_string = "Lib ssh error occurred while opening connection: " + std::string(errno_c);
    return err_code;
  }
  else
  {
    // no error
    std::cout << "Connection successfully open on " << ipAddress << " - " << tcpPort << std::endl;
  }

  // set gpascii mode
  size_t bytes = 0;
  char buff[512] = "";

  ssh_status = m_sshdriver->write(kPMAC_CONNECT_GPASCII.c_str(), 
    kPMAC_CONNECT_GPASCII.length(), &bytes, kPMAC_BOX_TIMEOUT);
  if (ssh_status != SSHDriverSuccess)
  {
    // error
    err_code = pmac_libssh_ERROR;
    char errno_c[10];
    sprintf(errno_c, "%d", ssh_status);

    this->m_error_string = "Lib ssh error occurred while setting gpascii mode: " + std::string(errno_c);
    return err_code;
  }

  // read answer
  ssh_status = m_sshdriver->read(buff, 512, &bytes, '\n', kPMAC_BOX_TIMEOUT);
  if (ssh_status != SSHDriverSuccess)
  {
    // error
    err_code = pmac_libssh_ERROR;
    char errno_c[10];
    sprintf(errno_c, "%d", ssh_status);

    this->m_error_string = "Lib ssh error occurred while reading gpascii reply: " + std::string(errno_c);
    return err_code;
  }

  // send echo7
  std::string echo_str = "echo7\n";
  ssh_status = m_sshdriver->write(echo_str.c_str(), 
    echo_str.length(), &bytes, kPMAC_BOX_TIMEOUT);
  if (ssh_status != SSHDriverSuccess)
  {
    // error
    err_code = pmac_libssh_ERROR;
    char errno_c[10];
    sprintf(errno_c, "%d", ssh_status);

    this->m_error_string = "Lib ssh error occurred while sending echo7: " + std::string(errno_c);
    return err_code;
  }

  // read answer
  ssh_status = m_sshdriver->read(buff, 512, &bytes, '\n', kPMAC_BOX_TIMEOUT);
  if (ssh_status != SSHDriverSuccess)
  {
    // error
    err_code = pmac_libssh_ERROR;
    char errno_c[10];
    sprintf(errno_c, "%d", ssh_status);

    this->m_error_string = "Lib ssh error occurred while reading echo7 reply: " + std::string(errno_c);
    return err_code;
  }

  m_is_connected = true;
  std::cout << "Connection parameters successfully set on " << ipAddress << " - " << tcpPort << std::endl;


#else
  m_sim_box_status.WDTFault0 = 0;
  m_sim_box_status.WDTFault1 = 0;
  m_sim_box_status.PwrOnFault = 0;

  std::cout << "Connection opened on " << ipAddress << " - " << tcpPort << std::endl;
  m_is_connected = true;

#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::close
// ============================================================================
E_pmac_errno SGonPMAC::close()
{
  std::cout << "SGonPMAC::close() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  
  CHECK_PMAC_CTRL;

  SSHDriverStatus ssh_status;

  // close connection only if opened
  if (m_is_connected)
  {
    ssh_status = this->m_sshdriver->disconnectSSH();

    if (ssh_status != SSHDriverSuccess)
    {
      // error
      err_code = pmac_libssh_ERROR;
      char errno_c[10];
      sprintf(errno_c, "%d", ssh_status);

      this->m_error_string = "Lib ssh error occurred while closing connection: " + std::string(errno_c);
    }
    else
    {
      // no error
      std::cout << "Connection closed successfully!" << std::endl;
      m_is_connected = false;
    }
  }
  else
  {
    // nothing to do, connection already closed
    std::cout << "Connection already closed!" << std::endl;
  }
#else
  m_sim_box_status.WDTFault0 = 1;
  m_sim_box_status.WDTFault1 = 1;
  m_sim_box_status.PwrOnFault = 1;

  std::cout << "Connection closed!" << std::endl;
  m_is_connected = false;
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_connection_state
// ============================================================================
E_pmac_errno SGonPMAC::get_connection_state(bool& state)
{
  //std::cout << "SGonPMAC::get_connection_state()" << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  

  // write global status command to check connection validity
  std::string reply;
  err_code = write_read(kPMAC_VAR_NAME_GLOBAL_STATUS, reply);
  if (err_code != pmac_NO_ERROR)
  {
    state = false;
  }
  else
  {
    state = true;
  }

#else
  state = m_is_connected;
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_raw_status
// ============================================================================
E_pmac_errno SGonPMAC::get_raw_status(T_ppmac_brick_status& status)
{
  //std::cout << "SGonPMAC::get_raw_status() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  

  // reset status
  unsigned int boxStatus;
  boxStatus = BOX_STATUS_NoERROR;
  
  // first get global status from box
  unsigned int uivalue = 0;

  std::string reply;
  err_code = write_read(kPMAC_VAR_NAME_CS_STATUS, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  // answer's like: $0
  // delete 1st character ($) and get next one
  char cstatus[4] = "";
  reply.copy(cstatus, 2, 1);
  if (sscanf(cstatus, "%1x", &uivalue) != 1)
  {
    err_code = pmac_CTRL_ERROR;
    this->m_error_string = "Unexpected PMAC reply for controller status request: " + reply;
    return err_code;
  }

  // compose box status
  if (uivalue & SYS_STATUS_BGWDTFault)
    boxStatus |= BOX_STATUS_BGWDTFault;

  if (uivalue & SYS_STATUS_RtIntWDTFault)
    boxStatus |= BOX_STATUS_RtIntWDTFault;   

  if (uivalue & SYS_STATUS_PwrOnFault)
    boxStatus |= BOX_STATUS_PwrOnFault;           

  if (uivalue & SYS_STATUS_ProjectLoadErr)
    boxStatus |= BOX_STATUS_ProjectLoadErr;  

  if (uivalue & SYS_STATUS_ConfigLoadErr)
    boxStatus |= BOX_STATUS_ConfigLoadErr;   

  if (uivalue & SYS_STATUS_HWChangeErr)
    boxStatus |= BOX_STATUS_HWChangeErr;           

  if (uivalue & SYS_STATUS_FileConfigErr)
    boxStatus |= BOX_STATUS_FileConfigErr;   

  if (uivalue & SYS_STATUS_Default) 
    boxStatus |= BOX_STATUS_Default;            

  if (uivalue & SYS_STATUS_NoClocks)
    boxStatus |= BOX_STATUS_NoClocks;                 
  

  // check AbortAll bit located in sys.status
  int ivalue = 0;
  err_code = write_read(kPMAC_VAR_NAME_BOX_STATUS_ABORT, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  {
    std::istringstream stream(reply);
    stream >> ivalue;
    if (stream.fail())
    {
      // Conversion failed
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for abort all status request: " + reply;
      return err_code;
    }
  }
  if (ivalue)
    boxStatus |= BOX_STATUS_AbortAll;

  // !!! No motor power supply will set BusOverVoltage and BusUnderVoltage status
  err_code = write_read(kPMAC_VAR_NAME_BOX_STATUS_OV, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  {
    std::istringstream stream(reply);
    stream >> ivalue;
    if (stream.fail())
    {
      // Conversion failed
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for voltage status request: " + reply;
      return err_code;
    }
  }
  if (ivalue)
    boxStatus |= BOX_STATUS_BusOverVoltage;

  // check over temperature
  err_code = write_read(kPMAC_VAR_NAME_BOX_STATUS_OT, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  {
    std::istringstream stream(reply);
    stream >> ivalue;
    if (stream.fail())
    {
      // Conversion failed
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for temperature status request: " + reply;
      return err_code;
    }
  }
  if (ivalue)
    boxStatus |= BOX_STATUS_BoardOverTemp;

  err_code = write_read(kPMAC_VAR_NAME_BOX_STATUS_BUV, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  {
    std::istringstream stream(reply);
    stream >> ivalue;
    if (stream.fail())
    {
      // Conversion failed
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for voltage status request: " + reply;
      return err_code;
    }
  }
  if (ivalue)
    boxStatus |= BOX_STATUS_BusUnderVoltage;

  // check kinematic errors
  err_code = write_read(kPMAC_VAR_NAME_BOX_FWD_KIN_ERR, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  {
    std::istringstream stream(reply);
    stream >> ivalue;
    if (stream.fail())
    {
      // Conversion failed
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for fwd kinematic status request: " + reply;
      return err_code;
    }
  }
  if (ivalue)
    boxStatus |= BOX_STATUS_FwdKinErr;

  err_code = write_read(kPMAC_VAR_NAME_BOX_INV_KIN_ERR, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  {
    std::istringstream stream(reply);
    stream >> ivalue;
    if (stream.fail())
    {
      // Conversion failed
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for inv kinematic status request: " + reply;
      return err_code;
    }
  }
  if (ivalue)
    boxStatus |= BOX_STATUS_InvKinErr;

  err_code = write_read(kPMAC_VAR_NAME_BOX_CALC_COMP_ERR, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  {
    std::istringstream stream(reply);
    stream >> ivalue;
    if (stream.fail())
    {
      // Conversion failed
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for calc comp status request: " + reply;
      return err_code;
    }
  }
  if (ivalue)
    boxStatus |= BOX_STATUS_CalcCompErr;

  T_ppmac_brick_status * val_pt = reinterpret_cast<T_ppmac_brick_status*>(&boxStatus);
  status = (*val_pt);
#else
  status = m_sim_box_status;
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_cpu_temperature
// ============================================================================
E_pmac_errno SGonPMAC::get_cpu_temperature(double& temp)
{
  //std::cout << "SGonPMAC::get_cpu_temperature() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  
  temp = 0.0;

  std::string reply;
  err_code = write_read(kPMAC_CPU_TEMP, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  {
    std::istringstream stream(reply);
    stream >> temp;
    if (stream.fail())
    {
      // Conversion failed
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for CPU temperature: " + reply;
      return err_code;
    }
  }

#else
  temp = 21.6;
#endif

  return err_code;
}

#if !defined (_SIMULATION_)
// ============================================================================
// SGonPMAC::compute_cpu_usage_info
// ============================================================================
E_pmac_errno SGonPMAC::compute_cpu_usage_info(double& phase_usage, double& servo_usage, 
                                        double& rti_usage, double& bg_usage)
{
  E_pmac_errno err_code = pmac_NO_ERROR;

	// Initialise values
	double Sys_FltrPhaseTime = 0;
	double Sys_FltrServoTime = 0;
	double Sys_FltrRtIntTime = 0;
	double Sys_FltrBgTime = 0;
	double Sys_BgSleepTime = 0;
	double Sys_PhaseDeltaTime = 0;
	double Sys_ServoDeltaTime = 0;
	double Sys_RtIntDeltaTime = 0;
	double Sys_BgDeltaTime = 0;

	phase_usage = 0;
	servo_usage = 0;
	rti_usage = 0;
	bg_usage = 0;


  // Build single request string
  std::string request_string = "Sys.FltrPhaseTime "
			"Sys.FltrServoTime "
			"Sys.FltrRtIntTime "
			"Sys.FltrBgTime "
			"Sys.BgSleepTime "
			"Sys.PhaseDeltaTime "
			"Sys.ServoDeltaTime "
			"Sys.RtIntDeltaTime "
			"Sys.BgDeltaTime\n";

	// Send command and read response
	std::string reply;
  err_code = write_read(request_string, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  // Extract responses
  std::stringstream extract(reply);

  extract >> Sys_FltrPhaseTime;
  extract >> Sys_FltrServoTime;
  extract >> Sys_FltrRtIntTime;
  extract >> Sys_FltrBgTime;
  extract >> Sys_BgSleepTime;
  extract >> Sys_PhaseDeltaTime;
  extract >> Sys_ServoDeltaTime;
  extract >> Sys_RtIntDeltaTime;
  extract >> Sys_BgDeltaTime;

  // Phase task time
  double phaseTaskTime_usec = Sys_FltrPhaseTime;

  // Servo task time
  double servoTaskTime_usec = Sys_FltrServoTime
				- ( (int) (Sys_FltrServoTime / Sys_PhaseDeltaTime) + 1 ) * phaseTaskTime_usec;
  
  if (servoTaskTime_usec > 0.0)
  {
    m_last_positive_servoTaskTime = servoTaskTime_usec;
  }
	else
	{
		servoTaskTime_usec = m_last_positive_servoTaskTime;
	}

	// RT Interrupt Task Time
	double rtIntTaskTime_usec = Sys_FltrRtIntTime
			- ( (int)(Sys_FltrRtIntTime / Sys_PhaseDeltaTime) + 1 ) * phaseTaskTime_usec
			- ( (int)(Sys_FltrRtIntTime / Sys_ServoDeltaTime) + 1 ) * servoTaskTime_usec;

	if (rtIntTaskTime_usec > 0.0)
	{
		m_last_positive_rtIntTaskTime = rtIntTaskTime_usec;
	}
	else
	{
		rtIntTaskTime_usec = m_last_positive_rtIntTaskTime;
	}

	// Background Task Time
	double temp_bgDeltaTime = Sys_FltrBgTime + Sys_FltrRtIntTime;
	double bgTaskTime_usec = temp_bgDeltaTime;

	double difference_bg_rti = ((int)(temp_bgDeltaTime / Sys_RtIntDeltaTime) + 1)
			* rtIntTaskTime_usec;

	if (bgTaskTime_usec > difference_bg_rti)
	{
		bgTaskTime_usec = bgTaskTime_usec - difference_bg_rti;
	}

	double difference_bg_servo
			= ((int)(temp_bgDeltaTime / Sys_ServoDeltaTime) + 1) * servoTaskTime_usec;

	if (bgTaskTime_usec > difference_bg_servo)
	{
		bgTaskTime_usec = bgTaskTime_usec - difference_bg_servo;
	}

	double difference_bg_phase
			= ((int)(temp_bgDeltaTime / Sys_PhaseDeltaTime) + 1) * phaseTaskTime_usec;

	if (bgTaskTime_usec > difference_bg_phase)
	{
		bgTaskTime_usec = bgTaskTime_usec - difference_bg_phase;
	}

	// If Sys.BgSleepTime is set to 0, it means use a value of 1000 us.
	if (Sys_BgSleepTime == 0)
	{
		Sys_BgSleepTime = 1000;
	}
	double temp_overallTime_usec = Sys_FltrRtIntTime + Sys_FltrBgTime + Sys_BgSleepTime;

	// Phase task usage %
	phase_usage
		= ((int)(temp_overallTime_usec / Sys_PhaseDeltaTime) + 1)
		* phaseTaskTime_usec / temp_overallTime_usec * 100;

	// Servo task usage %
	servo_usage
		= ((int)(temp_overallTime_usec / Sys_ServoDeltaTime) + 1)
		* servoTaskTime_usec / temp_overallTime_usec * 100;

	// RT interrupt task usage
	rti_usage
		= ((int)(temp_overallTime_usec / Sys_RtIntDeltaTime) + 1)
		* rtIntTaskTime_usec / temp_overallTime_usec * 100;

	// Background task usage
	bg_usage
		= bgTaskTime_usec / temp_overallTime_usec * 100;

  return err_code;
}
#endif

// ============================================================================
// SGonPMAC::get_cpu_running_time
// ============================================================================
E_pmac_errno SGonPMAC::get_cpu_running_time(double& cpu_time)
{
  //std::cout << "SGonPMAC::get_cpu_running_time() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  
  cpu_time = 0.0;

  std::string reply;
  err_code = write_read(kPMAC_RUN_TIME, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  {
    std::istringstream stream(reply);
    stream >> cpu_time;
    if (stream.fail())
    {
      // Conversion failed
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for CPU running time: " + reply;
      return err_code;
    }
  }

#else
  cpu_time = 99.9;
#endif
  return err_code;
}

// ============================================================================
// SGonPMAC::get_cpu_phase_usage
// ============================================================================
E_pmac_errno SGonPMAC::get_cpu_phase_usage(double& cpu_usage)
{
  //std::cout << "SGonPMAC::get_cpu_phase_usage() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  
  cpu_usage = 0.0;
  // update CPU data
  double dummy1;
  double dummy2;
  double dummy3;
  err_code = compute_cpu_usage_info(cpu_usage, dummy1, dummy2, dummy3);
  return err_code;

#else
  cpu_usage = 88.8;
#endif
  return err_code;
}

// ============================================================================
// SGonPMAC::get_cpu_servo_usage
// ============================================================================
E_pmac_errno SGonPMAC::get_cpu_servo_usage(double& cpu_usage)
{
  //std::cout << "SGonPMAC::get_cpu_servo_usage() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  
  cpu_usage = 0.0;

  // update CPU data
  double dummy1;
  double dummy2;
  double dummy3;
  err_code = compute_cpu_usage_info(dummy1, cpu_usage, dummy2, dummy3);
  return err_code;

#else
  cpu_usage = 77.7;
#endif
  return err_code;
}

// ============================================================================
// SGonPMAC::get_cpu_rti_usage
// ============================================================================
E_pmac_errno SGonPMAC::get_cpu_rti_usage(double& cpu_usage)
{
  //std::cout << "SGonPMAC::get_cpu_rti_usage() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  
  cpu_usage = 0.0;
  // update CPU data
  double dummy1;
  double dummy2;
  double dummy3;
  err_code = compute_cpu_usage_info(dummy1, dummy2, cpu_usage, dummy3);
  return err_code;

#else
  cpu_usage = 66.6;
#endif
  return err_code;
}

// ============================================================================
// SGonPMAC::get_cpu_bg_usage
// ============================================================================
E_pmac_errno SGonPMAC::get_cpu_bg_usage(double& cpu_usage)
{
  //std::cout << "SGonPMAC::get_cpu_bg_usage() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  
  cpu_usage = 0.0;

  // update CPU data
  double dummy1;
  double dummy2;
  double dummy3;
  err_code = compute_cpu_usage_info(dummy1, dummy2, dummy3, cpu_usage);
  return err_code;
#else
  cpu_usage = 55.5;
#endif
  return err_code;
}

// ============================================================================
// SGonPMAC::reset
// ============================================================================
E_pmac_errno SGonPMAC::reset()
{
  std::cout << "SGonPMAC::reset() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  
  std::string reply;
  err_code = write_read(kPMAC_CMD_NAME_RESET, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  // no error
  std::cout << "Reset command sent successfully!" << std::endl;

#else
  m_sim_box_status.WDTFault0 = 0;
  m_sim_box_status.WDTFault0 = 0;
  m_sim_box_status.PwrOnFault = 0;
  m_sim_box_status.ProjectLoadErr = 0 ;
  m_sim_box_status.ConfigLoadErr = 0;
  m_sim_box_status.HWChangeErr = 0;
  m_sim_box_status.FileConfigErr = 0;
  m_sim_box_status.Default = 0;
  m_sim_box_status.NoClocks = 0;
  m_sim_box_status.AbortAll = 0;
  m_sim_box_status.BusOverVoltage = 0;
  m_sim_box_status.OverTemp = 0;
  m_sim_box_status.BusUnderVoltage = 0;

  m_sim_box_status.NotUsed10 = 0;
  m_sim_box_status.NotUsed11 = 0;
  m_sim_box_status.NotUsed12 = 0;
  m_sim_box_status.FwdKinErr = 0;
  m_sim_box_status.InvKinErr = 0;
  m_sim_box_status.CalcCompErr = 0;

  m_sim_box_status.NotUsed19 = 0;
  m_sim_box_status.NotUsed20 = 0;
  m_sim_box_status.NotUsed21 = 0;
  m_sim_box_status.NotUsed22 = 0;
  m_sim_box_status.NotUsed23 = 0;
  m_sim_box_status.NotUsed24 = 0;
  m_sim_box_status.NotUsed25 = 0;
  m_sim_box_status.NotUsed26 = 0;
  m_sim_box_status.NotUsed27 = 0;
  m_sim_box_status.NotUsed28 = 0;
  m_sim_box_status.NotUsed29 = 0;
  m_sim_box_status.NotUsed30 = 0;
  m_sim_box_status.NotUsed31 = 0;

  std::cout << "DeltaTAU reset!" << std::endl;
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::reboot
// ============================================================================
E_pmac_errno SGonPMAC::reboot()
{
  std::cout << "SGonPMAC::reboot() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  
  std::string reply;
  err_code = write_read(kPMAC_CMD_NAME_REBOOT, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  // no error
  std::cout << "Reboot command sent successfully!" << std::endl;
#else
  m_sim_box_status.WDTFault0 = 1;
  m_sim_box_status.WDTFault1 = 1;
  m_sim_box_status.PwrOnFault = 1;

  std::cout << "DeltaTAU reboot!" << std::endl;
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::abort
// ============================================================================
E_pmac_errno SGonPMAC::abort()
{
  std::cout << "SGonPMAC::abort() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  
  std::string reply;
  err_code = write_read(kPMAC_CMD_NAME_ABORT, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  // no error
  std::cout << "abort command sent successfully!" << std::endl;
#else
  std::cout << "All CS movements aborted!" << std::endl;
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::send_cmd
// ============================================================================
E_pmac_errno SGonPMAC::send_cmd(std::string cmd, std::string& cmd_result)
{
  std::cout << "SGonPMAC::send_cmd() entering..." << std::endl;
  std::cout << "   cmd: " << cmd << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

  // check command validity
  if (cmd.empty())
  {
    // error
    err_code = pmac_range_ERROR;
    this->m_error_string = "Command not sent: empty string!";
    return err_code;
  }

#if !defined (_SIMULATION_) 
  std::string command = cmd;
  int length = command.length();

  if (command.at(length-1) != '\n')
  {
    command = command + "\n"; //Add the new line
  }
  
  err_code = write_read(command, cmd_result);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  // no error
  std::cout << "<" << cmd << "> command sent successfully! Reply = <" 
      << cmd_result << ">" << std::endl;
#else
  std::cout << "Command " << cmd << " sent!" << std::endl;
  cmd_result = "My simulated command result";
 
  if (cmd.compare("warning") == 0)
  {
    m_sim_box_status.BusUnderVoltage = 1;
  }
  if (cmd.compare("fault") == 0)
  {
    m_sim_box_status.BusOverVoltage = 1;
    m_sim_box_status.OverTemp = 1;
  }
#endif
  return err_code;
}

// ============================================================================
// SGonPMAC::get_firmware_rev
// ============================================================================
E_pmac_errno SGonPMAC::get_firmware_rev(std::string& vers)
{
  std::cout << "SGonPMAC::get_firmware_rev() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  

  err_code = write_read(kPMAC_VAR_NAME_VERS, vers);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  // no error
  std::cout << "Firmware version is: " << vers << std::endl;
#else
  vers = "SIM FIRM 1.0.0";
#endif
  return err_code;
}

// ============================================================================
// SGonPMAC::get_ucode_rev
// ============================================================================
E_pmac_errno SGonPMAC::get_ucode_rev(std::string& vers)
{
  std::cout << "SGonPMAC::get_ucode_rev() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  
  
  std::string ucode = "";
  err_code = write_read(kPMAC_VAR_NAME_VERS_MAJ, ucode);
  if (err_code != pmac_NO_ERROR)
    return err_code;
  vers = ucode + std::string(".");

  ucode = "";
  err_code = write_read(kPMAC_VAR_NAME_VERS_MIN, ucode);
  if (err_code != pmac_NO_ERROR)
    return err_code;
  vers = vers + ucode + std::string(".");

  ucode = "";
  err_code = write_read(kPMAC_VAR_NAME_VERS_SUB, ucode);
  if (err_code != pmac_NO_ERROR)
    return err_code;
  vers = vers + ucode;

  // no error
  std::cout << "UCode version is: " << vers << std::endl;

#else
  vers = "SIM UC 1.0.0";
#endif
  return err_code;
}

// ============================================================================
// SGonPMAC::init_axis
// ============================================================================
E_pmac_errno SGonPMAC::init_axis(T_axis_id id)
{
  std::cout << "SGonPMAC::init_axis() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

  // check axis name range:
  // the only axes available in SMARGON controller are:
  //   CS = 1
  //   axis name = A, B, C, X, Y or Z

  char cs_str[4];
  sprintf(cs_str, "%d", id.cs);

  if (id.cs != kSMARGON_CS_VALUE)
  {
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Bad CS value: " + std::string(cs_str);
    return err_code;
  }

  if (id.axis_name.compare(kPMAC_VAR_NAME_AXIS_A) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_B) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_C) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_X) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_Y) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_Z))
  {
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Bad axis name: " + id.axis_name;
    return err_code;
  }

#if !defined (_SIMULATION_) 
  double axis_pos = 0.0;

  // store axis reference in map
  std::pair<AxisPos_it_t, bool> ret_ins;
  ret_ins = this->m_axis_list.insert(std::pair<T_axis_id, double>(id, axis_pos));

  if (ret_ins.second == false) 
  {
    // error
    err_code = pmac_internal_ERROR;
    this->m_error_string = "Already declared axis: (" + std::string(cs_str) + std::string(", ")
      + id.axis_name + std::string(")!");
  }
#else
  CsParams default_cs;

  T_cs_status status;
  
  status.SyncAssignError = 0;
  status.BufferError = 0;
  status.LinToPvtError = 0;
  status.PvtError = 0;
  status.RunTimeError = 0;
  status.SoftLimit = 0;
  status.RadiusError = 0;
  status.RadiusError2 = 0;

  status.TimersEnabled = 1;
  status.BlockRequest = 1;
  status.NotUsed1 = 0;
  status.InPos = 1;
  status.AmpEna = 1;
  status.ClosedLoop = 1;
  status.DesVelZero = 0;
  status.HomeComplete = 0;

  status.TimerEnabled = 0;
  status.NotUsed2 = 0;
  status.EncLoss = 0;
  status.AmpWarn = 0;
  status.TriggerNotFound = 0;
  status.I2tFault = 0;
  status.SoftPlusLimit = 0;
  status.SoftMinusLimit = 0;

  status.AmpFault = 0;
  status.LimitStop = 0;
  status.FeFatal = 0;
  status.FeWarn = 0;
  status.PlusLimit = 1;
  status.MinusLimit = 1;
  status.HomeInProgress = 0;
  status.TriggerMove = 0;
  
  default_cs.status = status;

  default_cs.velocity = 1.0;
  default_cs.accel_time = 880.0;
  default_cs.decel_time = -90.0;
  default_cs.mv_mode = CS_MV_MODE_LINEAR;
  default_cs.scurve_time = 100.0;

  if (this->m_sim_cs_param.count(id.cs) == 0)
  {
    this->m_sim_cs_param.insert( std::pair<T_cs_id, CsParams>(id.cs, default_cs) ); 
  }


  AxisParams default_axis_param;
  default_axis_param.offset = -0.5;
  default_axis_param.compens = false;

  if (this->m_sim_axis_params.count(id) == 0)
  {
    this->m_sim_axis_params.insert( std::pair<T_axis_id, AxisParams>(id, default_axis_param) );
  }
  else
  {
    std::cout << "Error!! Axis already in map!" << std::endl;
    err_code = pmac_configuration_ERROR;
  }

  AxisState default_axis_st;
  default_axis_st.position = 0.0;
  default_axis_st.pos_setpoint = 0.0;

  if (this->m_sim_axis_state.count(id) == 0)
  {
    this->m_sim_axis_state.insert( std::pair<T_axis_id, AxisState>(id, default_axis_st) );
  }
  else
  {
    std::cout << "Error!! Axis already in map!" << std::endl;
    err_code = pmac_configuration_ERROR;
  }

  // print maps
  /*
    std::map<T_axis_id, AxisParams>::iterator itr;
    for (itr = this->m_sim_axis_params.begin();
         itr != this->m_sim_axis_params.end();
         ++itr)
    {
      std::cout << "cs, axis, offset, backlash ==> " << 
      (itr->first).cs << ", " << (itr->first).axis_name << ", " <<
      (itr->second).offset << ", " << (itr->second).backlash << std::endl;
    }

    std::map<T_axis_id, AxisState>::iterator itr2;
    for (itr2 = this->m_sim_axis_state.begin();
         itr2 != this->m_sim_axis_state.end();
         ++itr2)
    {
       std::cout << "cs, axis, position, pos_setpoint ==> " << 
      (itr2->first).cs << ", " << (itr2->first).axis_name << ", " <<
      (itr2->second).position << ", " << (itr2->second).pos_setpoint << std::endl;
    }
  */

#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::undeclare_axis
// ============================================================================
E_pmac_errno SGonPMAC::undeclare_axis(T_axis_id id)
{
  std::cout << "SGonPMAC::undeclare_axis() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;

#if !defined (_SIMULATION_)  
  
  if (this->m_axis_list.count(id) != 0)
  {
    // delete element from list
    this->m_axis_list.erase(id);
  }
  else
  {
    // error: element not found in list
    err_code = pmac_internal_ERROR;
    char cs_str[4];
    sprintf(cs_str, "%d", id.cs);
    this->m_error_string = "undeclared axis: (" + std::string(cs_str) + std::string(", ")
      + id.axis_name + std::string(")!");
  }
#else
  // delete elements from lists
  if (this->m_sim_cs_param.count(id.cs) != 0)
  {
    this->m_sim_cs_param.erase(id.cs); 
  }

  if (this->m_sim_axis_params.count(id) != 0)
  {
    this->m_sim_axis_params.erase(id);
  }

  if (this->m_sim_axis_state.count(id) != 0)
  {
    this->m_sim_axis_state.erase(id);
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_cs_status
// ============================================================================
E_pmac_errno SGonPMAC::get_cs_status(T_cs_id id, T_cs_status& status)
{
  //std::cout << "SGonPMAC::get_cs_status() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd = kPMAC_VAR_NAME_CS_COORD + std::string(cs_str) + 
      kPMAC_VAR_NAME_CMD_SEP + kPMAC_VAR_NAME_STATUS + std::string("[0]\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
    
    // answer's like: $00FF0AC1

    // delete 1st character ($) and get 8 next ones
    char cstatus[8] = "";
    reply.copy(cstatus, 8, 1);

    // convert to hexa bit field
    uint32_t h;
    if (sscanf(cstatus, "%8x", &h) != 1)
    {
      // Conversion failed
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for CS status request: " + reply;
      return err_code;
    }

    T_cs_status * val_pt = reinterpret_cast<T_cs_status*>(&h);
    status = (*val_pt);   
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }

#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    status = this->m_sim_cs_param[id].status;
    if (m_sim_mp_state)
      status.TimerEnabled = 1;
    else
      status.TimerEnabled = 0;
    
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_cs_motors_status
// ============================================================================
E_pmac_errno SGonPMAC::get_cs_motors_status(T_cs_id id, std::string& status)
{
  //std::cout << "SGonPMAC::get_cs_motors_status() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);
  status = "";

#if !defined (_SIMULATION_)  

  if (this->is_cs_declared(id))
  {
    std::string reply = "";
    std::string cmd = "";
    char motor_str[4];

    // get motor status from motor 1 to 6
    for (size_t idm = 1; idm < 7; idm++)
    {
      sprintf(motor_str, "%d", idm);
      cmd = kPMAC_VAR_NAME_MOTOR + std::string(motor_str) + 
        kPMAC_VAR_NAME_CMD_SEP + kPMAC_VAR_NAME_STATUS + std::string("[0]\n");

      status += std::string("\nMotor #") + std::string(motor_str) + std::string (" status:\n");

      err_code = write_read(cmd, reply);
      if (err_code != pmac_NO_ERROR)
        return err_code;
    
      // answer's like: $00FF0AC1

      // delete 1st character ($) and get 8 next ones
      char mttatus[8] = "";
      reply.copy(mttatus, 8, 1);

      // convert to hexa bit field
      uint32_t h;
      if (sscanf(mttatus, "%8x", &h) != 1)
      {
        // Conversion failed
        // error
        err_code = pmac_CTRL_ERROR;
        this->m_error_string = "Unexpected PMAC reply for motor status request: " + reply;
        return err_code;
      }

      T_motor_status * val_pt = reinterpret_cast<T_motor_status*>(&h);
      T_motor_status raw_mt_status = (*val_pt);

      // if bit set to 1, write in status
      status += analyze_motor_status(raw_mt_status);
    }
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }

#else

  if (this->m_sim_cs_param.count(id) != 0)
  {
    status = std::string("Motor #1: In position\n") + std::string("Motor #6: Amplifier enabled");
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }

#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::analyze_motor_status
// ============================================================================
std::string SGonPMAC::analyze_motor_status(T_motor_status raw_status)
{
  std::string status_str = "";
  
  if (raw_status.TriggerMove)
    status_str += "Trigger search move in progress\n";
  if (raw_status.HomeInProgress)
    status_str += "Home search move in progress\n";
  if (raw_status.MinusLimit)
    status_str += "Hardware negative limit set\n";
  if (raw_status.PlusLimit)
    status_str += "Hardware positive limit set\n";
  if (raw_status.FeWarn)
    status_str += "Warning following error\n";
  if (raw_status.FeFatal)
    status_str += "Fatal following error\n";
  if (raw_status.LimitStop)
    status_str += "Stopped on hardware limit\n";
  if (raw_status.AmpFault)
    status_str += "Amplifier fault\n";
  if (raw_status.SoftMinusLimit)
    status_str += "Software negative limit set\n";
  if (raw_status.SoftPlusLimit)
    status_str += "Software positive limit set\n";
  if (raw_status.I2tFault)
    status_str += "Integrated current (I2T) fault\n";
  if (raw_status.TriggerNotFound)
    status_str += "Trigger not found\n";
  if (raw_status.AmpWarn)
    status_str += "Amplifier warning\n";
  if (raw_status.EncLoss)
    status_str += "Sensor loss error\n";
  if (raw_status.HomeComplete)
    status_str += "Home complete\n";
  if (raw_status.DesVelZero)
    status_str += "Desired velocity zero\n";
  if (raw_status.ClosedLoop)
    status_str += "Closed-loop mode\n";
  if (raw_status.AmpEna)
    status_str += "Amplifier enabled\n";
  if (raw_status.InPos)
    status_str += "In position\n";
  if (raw_status.BlockRequest)
    status_str += "Block request flag set\n";
  if (raw_status.PhaseFound)
    status_str += "Phase reference established\n";
  if (raw_status.TriggerSpeedSel)
    status_str += "Triggered move speed selected\n";
  if (raw_status.GantryHomed)
    status_str += "Gantry homing complete\n";
  if (raw_status.SpindleMotor1)
    status_str += "Spindle axis definition status (bit 1)\n";
  if (raw_status.SpindleMotor0)
    status_str += "Spindle axis definition status (bit 0)\n";

  return status_str;
}

// ============================================================================
// SGonPMAC::get_cs_ref_status
// ============================================================================
E_pmac_errno SGonPMAC::get_cs_ref_status(T_cs_id id, E_cs_referencing_status_t& status)
{
  //std::cout << "SGonPMAC::get_cs_ref_status() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)

  if (this->is_cs_declared(id))
  {
    int ref_st = 0;
    std::string reply;
    err_code = write_read(kPMAC_VAR_NAME_CS_REF_STATUS, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;

    {
      std::istringstream stream(reply);
      stream >> ref_st;
      if (stream.fail())
      {
        // Conversion failed
        // error
        err_code = pmac_CTRL_ERROR;
        this->m_error_string = "Unexpected PMAC reply for CS referencing status: " + reply;
        return err_code;
      }
    }
    // no error
    // conversion int --> enum
	if (ref_st == 0)
	  status = CS_REF_POS_UNREF;
	else if (ref_st == 6)
	  status = CS_REF_POS_OK;
	else if (ref_st < 0)
	  status = CS_REF_POS_KO;
	else if ((ref_st > 0) && (ref_st < 6))
	  status = CS_REF_POS_RUNNING;
	else 
	  status = CS_REF_POS_UNKNOWN;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }

#else

  if (this->m_sim_cs_param.count(id) != 0)
  {
    status = this->m_sim_cs_param[id].ref_status;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::motor_on
// ============================================================================
E_pmac_errno SGonPMAC::motor_on(T_cs_id id)
{
  std::cout << "SGonPMAC::motor_on() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)
  if (this->is_cs_declared(id))
  {
    std::string reply;
    err_code = write_read(kPMAC_CMD_NAME_ON, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    std::cout << "SGonPMAC CS " << id << " all motors powered ON! " << std::endl;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::motor_off
// ============================================================================
E_pmac_errno SGonPMAC::motor_off(T_cs_id id)
{
  std::cout << "SGonPMAC::motor_off() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  

  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd;
    cmd = kPMAC_CMD_NAME_OFF_BASE + std::string (" ") + kPMAC_CMD_NAME_MOTOR_ALL;

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    std::cout << "SGonPMAC CS " << id << " all motors powered OFF! " << std::endl;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::motor_omega_off
// ============================================================================
E_pmac_errno SGonPMAC::motor_omega_off(T_cs_id id)
{
  std::cout << "SGonPMAC::motor_omega_off() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  

  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd;
    cmd = kPMAC_CMD_NAME_OFF_BASE + std::string (" ") + kPMAC_CMD_NAME_MOTOR_OMEGA;

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    std::cout << "SGonPMAC CS " << id << " OMEGA motor powered OFF! " << std::endl;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::motor_other_off
// ============================================================================
E_pmac_errno SGonPMAC::motor_other_off(T_cs_id id)
{
  std::cout << "SGonPMAC::motor_other_off() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  

  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd;
    cmd = kPMAC_CMD_NAME_OFF_BASE + std::string (" ") + kPMAC_CMD_NAME_MOTOR_OTHER;

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    //this->m_sim_cs_param[id].state = CS_STATE_OFF;
    std::cout << "SGonPMAC CS " << id << " OTHER motors powered OFF! " << std::endl;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::axis_stop
// ============================================================================
E_pmac_errno SGonPMAC::axis_stop(T_cs_id id)
{
  std::cout << "SGonPMAC::axis_stop() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    std::string reply;

    err_code = write_read(kPMAC_VAR_NAME_CS_ABORT, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    std::cout << "SGonPMAC CS " << id << " all motors stopped! " << std::endl;

    std::map<T_axis_id, AxisState>::iterator itr;
    for (itr = this->m_sim_axis_state.begin();
         itr != this->m_sim_axis_state.end();
         ++itr)
    {
      if ((itr->first).cs == id)
      {
        // set axis position
        (itr->second).position = 66.6;
      }
    }
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::axis_init_ref_pos
// ============================================================================
E_pmac_errno SGonPMAC::axis_init_ref_pos(T_cs_id id)
{
  std::cout << "SGonPMAC::axis_init_ref_pos() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    std::string reply;

    err_code = write_read(kPMAC_VAR_NAME_CS_INIT_REF, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    std::map<T_axis_id, AxisParams>::iterator itr;
    for (itr = this->m_sim_axis_params.begin();
         itr != this->m_sim_axis_params.end();
         ++itr)
    {
      if ((itr->first).cs == id)
      {
        // initialize axis position with stored init position for all underlying 
        // axes of the CS
        T_axis_id axis;
        axis.cs = id;
        axis.axis_name = (itr->first).axis_name;
        this->m_sim_axis_state[axis].position = 66.6;
        std::cout << "SGonPMAC Axis (" << id << ", " << axis.axis_name << ") takes its ref position! " << std::endl;
      }
    }

    // init referencing status
    this->m_sim_cs_param[id].ref_status = CS_REF_POS_OK;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;

    // init referencing status
    this->m_sim_cs_param[id].ref_status = CS_REF_POS_KO;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::set_axis_position
// ============================================================================
E_pmac_errno SGonPMAC::set_axis_position(T_axis_id id, double position, bool apply)
{
  std::cout << "SGonPMAC::set_axis_position() entering... apply = " << apply << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id.cs);

#if !defined (_SIMULATION_)  

  if (this->m_axis_list.count(id) != 0)
  {
    // in omega case, inverse rotation direction
	if (id.axis_name.compare(kPMAC_VAR_NAME_AXIS_A) == 0)
	{
	  position = -1.0 * position;
	}
	
    // Apply position setpoint if required
    if (apply)
    {
      std::string reply;
      std::string cmd;
      std::string pos_str = yat::XString<double>::to_string(position); 
      cmd = kPMAC_VAR_NAME_AX_SETPOS + std::string(" ") 
        + id.axis_name + pos_str + std::string("\n");

      std::cout << "SGonPMAC::set_axis_position() send command: " << cmd << std::endl;
      err_code = write_read(cmd, reply);
      if (err_code != pmac_NO_ERROR)
        return err_code;
    }
    else
    {
      // memorize value for next apply command
      m_axis_list[id] = position;
    }
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared axis: (" + std::string(cs_str) + std::string(", ")
      + id.axis_name + std::string(")!");
  }
#else
  if ((this->m_sim_axis_state.count(id) != 0) &&
      (this->m_sim_cs_param.count(id.cs) != 0))
  {
    std::cout << "SGonPMAClib Axis " << id.axis_name << " position set to " << position << std::endl;
    this->m_sim_axis_state[id].pos_setpoint = position;

    if (apply)
    {
      //Sleep(1500); // win32
	  sleep(1.500); // linux
      this->m_sim_axis_state[id].position = this->m_sim_axis_state[id].pos_setpoint;
    }
  }
  else
  {
    std::string axis_str = std::string("(") + yat::XString<unsigned int>::to_string(id.cs) + 
      std::string(", ") + id.axis_name + std::string(")");
    m_error_string = "Axis " + axis_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::apply_axis_position
// ============================================================================
E_pmac_errno SGonPMAC::apply_axis_position(T_cs_id id)
{
  std::cout << "SGonPMAC::apply_axis_position() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    // apply memorized values for all axes
    std::string reply;
    std::string cmd = kPMAC_VAR_NAME_CS_SETPOS + std::string(" ");
    T_axis_id axis;
    axis.cs = id;

    axis.axis_name = kPMAC_VAR_NAME_AXIS_A;
    std::string pos_str = yat::XString<double>::to_string(m_axis_list[axis]); 
    cmd += axis.axis_name + pos_str + std::string(" ");
    axis.axis_name = kPMAC_VAR_NAME_AXIS_B;
    pos_str = yat::XString<double>::to_string(m_axis_list[axis]); 
    cmd += axis.axis_name + pos_str + std::string(" ");
    axis.axis_name = kPMAC_VAR_NAME_AXIS_C;
    pos_str = yat::XString<double>::to_string(m_axis_list[axis]); 
    cmd += axis.axis_name + pos_str + std::string(" ");
    axis.axis_name = kPMAC_VAR_NAME_AXIS_X;
    pos_str = yat::XString<double>::to_string(m_axis_list[axis]); 
    cmd += axis.axis_name + pos_str + std::string(" ");
    axis.axis_name = kPMAC_VAR_NAME_AXIS_Y;
    pos_str = yat::XString<double>::to_string(m_axis_list[axis]); 
    cmd += axis.axis_name + pos_str + std::string(" ");
    axis.axis_name = kPMAC_VAR_NAME_AXIS_Z;
    pos_str = yat::XString<double>::to_string(m_axis_list[axis]); 
    cmd += axis.axis_name + pos_str + std::string("\n");

	std::cout << "SGonPMAC::apply_axis_position() send command: " << cmd << std::endl;		
    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    //Sleep(1500); // win32
	sleep(1.5); // linux

    std::map<T_axis_id, AxisState>::iterator itr;
    for (itr = this->m_sim_axis_state.begin();
         itr != this->m_sim_axis_state.end();
         ++itr)
    {
      T_axis_id axis = (itr->first);
      if (axis.cs == id)
      {
          (itr->second).position = (itr->second).pos_setpoint;
      }
    }
  }
  else
  {
    std::string axis_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + 
      std::string(")");
    m_error_string = "Axis " + axis_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::set_axis_pos_offset
// ============================================================================
E_pmac_errno SGonPMAC::set_axis_pos_offset(T_axis_id id, double offset)
{
  std::cout << "SGonPMAC::set_axis_pos_offset() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id.cs);

  // check axis name: offset only available for X, Y or Z axes
  if (id.axis_name.compare(kPMAC_VAR_NAME_AXIS_X) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_Y) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_Z))
  {
    err_code = pmac_range_ERROR;
    this->m_error_string = "Bad axis name for position offset: only available for X, Y or Z axis";
    return err_code;
  }

#if !defined (_SIMULATION_)  
  if (this->m_axis_list.count(id) != 0)
  {
    std::string reply;
    std::string cmd;
    std::string ofs_str = yat::XString<double>::to_string(offset);
    std::string t = id.axis_name;
    std::transform(t.begin(), t.end(), t.begin(), ::tolower);
    cmd = t + kPMAC_VAR_NAME_AX_OFFSET + std::string("=") 
      + ofs_str + std::string("\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared axis: (" + std::string(cs_str) + std::string(", ")
      + id.axis_name + std::string(")!");
  }
#else
  if (this->m_sim_axis_params.count(id) != 0)
  {
    this->m_sim_axis_params[id].offset = offset;
  }
  else
  {
    std::string axis_str = std::string("(") + yat::XString<unsigned int>::to_string(id.cs) + 
      std::string(", ") + id.axis_name + std::string(")");
    m_error_string = "Axis " + axis_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_axis_pos_offset
// ============================================================================
E_pmac_errno SGonPMAC::get_axis_pos_offset(T_axis_id id, double& offset)
{
  //std::cout << "SGonPMAC::get_axis_pos_offset() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id.cs);

  // check axis name: offset only available for X, Y or Z axes
  if (id.axis_name.compare(kPMAC_VAR_NAME_AXIS_X) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_Y) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_Z))
  {
    offset = yat::IEEE_NAN;
    return err_code;
  }

#if !defined (_SIMULATION_)  

  if (this->m_axis_list.count(id) != 0)
  {
    std::string reply;
    std::string cmd;
    std::string t = id.axis_name;
    std::transform(t.begin(), t.end(), t.begin(), ::tolower);
    cmd = t + kPMAC_VAR_NAME_AX_OFFSET + std::string("\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;

    offset = 0.0;
    {
      std::istringstream stream(reply);
      stream >> offset;
      if (stream.fail())
      {
        // Conversion failed
        // error
        err_code = pmac_CTRL_ERROR;
        this->m_error_string = "Unexpected PMAC reply for offset value: " + reply;
        return err_code;
      }
    }
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared axis: (" + std::string(cs_str) + std::string(", ")
      + id.axis_name + std::string(")!");
  }
#else
  if (this->m_sim_axis_params.count(id) != 0)
  {
    std::string cmd;
    std::string t = id.axis_name;
    std::transform(t.begin(), t.end(), t.begin(), ::tolower);
    cmd = t + kPMAC_VAR_NAME_AX_OFFSET + std::string("\n");

    offset = this->m_sim_axis_params[id].offset;
  }
  else
  {
    std::string axis_str = std::string("(") + yat::XString<unsigned int>::to_string(id.cs) + 
      std::string(", ") + id.axis_name + std::string(")");
    m_error_string = "Axis " + axis_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::set_axis_compensation
// ============================================================================
E_pmac_errno SGonPMAC::set_axis_compensation(T_axis_id id, bool enabled)
{
  std::cout << "SGonPMAC::set_axis_compensation() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id.cs);

  // check axis name: compensation only available for A, B or C axis
  if (id.axis_name.compare(kPMAC_VAR_NAME_AXIS_A) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_B) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_C))
  {
    err_code = pmac_range_ERROR;
    this->m_error_string = "Bad axis name for compensation table: only available for A, B or C axis";
    return err_code;
  }

#if !defined (_SIMULATION_)  
  if (this->m_axis_list.count(id) != 0)
  {
    std::string reply;
    std::string cmd;
    cmd = kPMAC_VAR_NAME_AX_COMPENS_1 + id.axis_name + kPMAC_VAR_NAME_AX_COMPENS_2 
      + std::string("=");
    if (enabled)
      cmd += std::string("1\n");
    else
      cmd += std::string("0\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared axis: (" + std::string(cs_str) + std::string(", ")
      + id.axis_name + std::string(")!");
  }
#else
  if (this->m_sim_axis_params.count(id) != 0)
  {
    this->m_sim_axis_params[id].compens = enabled;
  }
  else
  {
    std::string axis_str = std::string("(") + yat::XString<unsigned int>::to_string(id.cs) + 
      std::string(", ") + id.axis_name + std::string(")");
    m_error_string = "Axis " + axis_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_axis_compensation
// ============================================================================
E_pmac_errno SGonPMAC::get_axis_compensation(T_axis_id id, bool& enabled)
{
  //std::cout << "SGonPMAC::get_axis_compensation() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id.cs);

  // check axis name: compensation only available for A, B or C axis
  if (id.axis_name.compare(kPMAC_VAR_NAME_AXIS_A) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_B) &&
      id.axis_name.compare(kPMAC_VAR_NAME_AXIS_C))
  {
    enabled = false;
    return err_code;
  }

#if !defined (_SIMULATION_)  
  if (this->m_axis_list.count(id) != 0)
  {
    std::string reply;
    std::string cmd;
    cmd = kPMAC_VAR_NAME_AX_COMPENS_1 + id.axis_name + kPMAC_VAR_NAME_AX_COMPENS_2 
      + std::string("\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;

    int tof = 0;
    {
      std::istringstream stream(reply);
      stream >> tof;
      if (stream.fail())
      {
        // Conversion failed
        // error
        err_code = pmac_CTRL_ERROR;
        this->m_error_string = "Unexpected PMAC reply for compensation status: " + reply;
        return err_code;
      }
      if (tof == 0)
        enabled = false;
      else if (tof == 1)
        enabled = true;
      else
      {
        // error
        err_code = pmac_CTRL_ERROR;
        this->m_error_string = "Unexpected PMAC reply for compensation status: " + reply;
        return err_code;
      }
    }
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared axis: (" + std::string(cs_str) + std::string(", ")
      + id.axis_name + std::string(")!");
  }
#else
  if (this->m_sim_axis_params.count(id) != 0)
  {
    std::string cmd;
    cmd = kPMAC_VAR_NAME_AX_COMPENS_1 + id.axis_name + kPMAC_VAR_NAME_AX_COMPENS_2 
      + std::string("\n");
    enabled = this->m_sim_axis_params[id].compens;
  }
  else
  {
    std::string axis_str = std::string("(") + yat::XString<unsigned int>::to_string(id.cs) + 
      std::string(", ") + id.axis_name + std::string(")");
    m_error_string = "Axis " + axis_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_cs_compens_valid
// ============================================================================
E_pmac_errno SGonPMAC::get_cs_compens_valid(T_cs_id id, bool& valid)
{
  //std::cout << "SGonPMAC::get_cs_compens_valid() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    std::string reply;

    err_code = write_read(kPMAC_VAR_NAME_AX_COMPENS_ST, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;

    int tof = 0;
    {
      std::istringstream stream(reply);
      stream >> tof;
      if (stream.fail())
      {
        // Conversion failed
        // error
        err_code = pmac_CTRL_ERROR;
        this->m_error_string = "Unexpected PMAC reply for compensation validity: " + reply;
        return err_code;
      }
      valid = (bool)tof;
    }
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    valid = this->m_sim_cs_param[id].compens_valid;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::set_cs_velocity
// ============================================================================
E_pmac_errno SGonPMAC::set_cs_velocity(T_cs_id id, double velocity)
{
  std::cout << "SGonPMAC::set_cs_velocity() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

  // check velocity value
  if (velocity < 0.0)
  {
    // error
    err_code = pmac_range_ERROR;
    this->m_error_string = "Value out of range: velocity should be a positive value!";
    return err_code;
  }

  // Apply coeff -1 to send a negative value to controller
  velocity = velocity * (-1.0);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd;
    std::string vel_str = yat::XString<double>::to_string(velocity);
    cmd = kPMAC_VAR_NAME_CS_COORD + std::string(cs_str) + kPMAC_VAR_NAME_CMD_SEP 
      + kPMAC_VAR_NAME_CS_SET_VEL + std::string("=") + vel_str + std::string("\n");
 
    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    this->m_sim_cs_param[id].velocity = velocity;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_cs_velocity
// ============================================================================
E_pmac_errno SGonPMAC::get_cs_velocity(T_cs_id id, double& velocity)
{
  //std::cout << "SGonPMAC::get_cs_velocity() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd;
    cmd = kPMAC_VAR_NAME_CS_COORD + std::string(cs_str) + kPMAC_VAR_NAME_CMD_SEP 
      + kPMAC_VAR_NAME_CS_SET_VEL + std::string("\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;

    {
      std::istringstream stream(reply);
      stream >> velocity;
      if (stream.fail())
      {
        // Conversion failed
        // error
        err_code = pmac_CTRL_ERROR;
        this->m_error_string = "Unexpected PMAC reply for velocity: " + reply;
        return err_code;
      }
      // Apply coeff -1 to send a positive value to client
      velocity = velocity * (-1.0);
    }
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    velocity = this->m_sim_cs_param[id].velocity;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::set_cs_acceleration
// ============================================================================
E_pmac_errno SGonPMAC::set_cs_acceleration(T_cs_id id, double acceleration)
{
  std::cout << "SGonPMAC::set_cs_acceleration() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

  // check acceleration value
  if (acceleration < 0.0)
  {
    // error
    err_code = pmac_range_ERROR;
    this->m_error_string = "Value out of range: acceleration should be a positive value!";
    return err_code;
  }

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd;
    std::string accel_str = yat::XString<double>::to_string(acceleration);
    cmd = kPMAC_VAR_NAME_CS_COORD + std::string(cs_str) + kPMAC_VAR_NAME_CMD_SEP 
      + kPMAC_VAR_NAME_CS_SET_ACCEL + std::string("=") + accel_str + std::string("\n");
 
    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    this->m_sim_cs_param[id].accel_time = acceleration;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_cs_acceleration
// ============================================================================
E_pmac_errno SGonPMAC::get_cs_acceleration(T_cs_id id, double& acceleration)
{
  //std::cout << "SGonPMAC::get_cs_acceleration() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd;
    cmd = kPMAC_VAR_NAME_CS_COORD + std::string(cs_str) + kPMAC_VAR_NAME_CMD_SEP 
      + kPMAC_VAR_NAME_CS_SET_ACCEL + std::string("\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;

    {
      std::istringstream stream(reply);
      stream >> acceleration;
      if (stream.fail())
      {
        // Conversion failed
        // error
        err_code = pmac_CTRL_ERROR;
        this->m_error_string = "Unexpected PMAC reply for acceleration: " + reply;
        return err_code;
      }
    }
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    acceleration = this->m_sim_cs_param[id].accel_time;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::set_cs_deceleration
// ============================================================================
E_pmac_errno SGonPMAC::set_cs_deceleration(T_cs_id id, double deceleration)
{
  std::cout << "SGonPMAC::set_cs_deceleration() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

  // check deceleration value
  if (deceleration < 0.0)
  {
    // error
    err_code = pmac_range_ERROR;
    this->m_error_string = "Value out of range: deceleration should be a positive value!";
    return err_code;
  }

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd;
    std::string decel_str = yat::XString<double>::to_string(deceleration);
    cmd = kPMAC_VAR_NAME_CS_COORD + std::string(cs_str) + kPMAC_VAR_NAME_CMD_SEP 
      + kPMAC_VAR_NAME_CS_SET_DECEL + std::string("=") + decel_str + std::string("\n");
 
    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    this->m_sim_cs_param[id].decel_time = deceleration;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_cs_deceleration
// ============================================================================
E_pmac_errno SGonPMAC::get_cs_deceleration(T_cs_id id, double& deceleration)
{
  //std::cout << "SGonPMAC::get_cs_deceleration() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd;
    cmd = kPMAC_VAR_NAME_CS_COORD + std::string(cs_str) + kPMAC_VAR_NAME_CMD_SEP 
      + kPMAC_VAR_NAME_CS_SET_DECEL + std::string("\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;

    {
      std::istringstream stream(reply);
      stream >> deceleration;
      if (stream.fail())
      {
        // Conversion failed
        // error
        err_code = pmac_CTRL_ERROR;
        this->m_error_string = "Unexpected PMAC reply for deceleration: " + reply;
        return err_code;
      }
    }
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    deceleration = this->m_sim_cs_param[id].decel_time;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::set_cs_scurve
// ============================================================================
E_pmac_errno SGonPMAC::set_cs_scurve(T_cs_id id, double scurve)
{
  std::cout << "SGonPMAC::set_cs_scurve() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

  // check scurve value
  if (scurve < 0.0)
  {
    // error
    err_code = pmac_range_ERROR;
    this->m_error_string = "Value out of range: scurve should be a positive value!";
    return err_code;
  }

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd;
    std::string sc_str = yat::XString<double>::to_string(scurve);
    cmd = kPMAC_VAR_NAME_CS_COORD + std::string(cs_str) + kPMAC_VAR_NAME_CMD_SEP 
      + kPMAC_VAR_NAME_CS_SET_SCURVE + std::string("=") + sc_str + std::string("\n");
 
    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    this->m_sim_cs_param[id].scurve_time = scurve;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_cs_scurve
// ============================================================================
E_pmac_errno SGonPMAC::get_cs_scurve(T_cs_id id, double& scurve)
{
  //std::cout << "SGonPMAC::get_cs_scurve() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd;
    cmd = kPMAC_VAR_NAME_CS_COORD + std::string(cs_str) + kPMAC_VAR_NAME_CMD_SEP 
      + kPMAC_VAR_NAME_CS_SET_SCURVE + std::string("\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;

    {
      std::istringstream stream(reply);
      stream >> scurve;
      if (stream.fail())
      {
        // Conversion failed
        // error
        err_code = pmac_CTRL_ERROR;
        this->m_error_string = "Unexpected PMAC reply for scurve: " + reply;
        return err_code;
      }
    }
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    scurve = this->m_sim_cs_param[id].scurve_time;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_axis_position
// ============================================================================
E_pmac_errno SGonPMAC::get_axes_positions(std::vector<T_axis_id> ids, std::vector<double>& pos_list)
{
  //std::cout << "SGonPMAC::get_axes_positions() entering..." << std::endl;
  /*std::cout << "list of axes to get position: " << std::endl;
  for (size_t idx = 0; idx < ids.size(); idx++)
    std::cout << ids[idx].axis_name << std::endl;*/

  E_pmac_errno err_code = pmac_NO_ERROR;
  pos_list.clear();

#if !defined (_SIMULATION_)  

  // get axes position from hardware:
  std::string reply;
  std::string cmd;
  cmd = std::string("&") + yat::XString<unsigned int>::to_string(kSMARGON_CS_VALUE) 
    + std::string("p\n");

  err_code = write_read(cmd, reply);
  if (err_code != pmac_NO_ERROR)
    return err_code;

  //std::cout << "reply for axis positions = " << reply << std::endl;
  // decrypt answer: A<pos> B<pos> C<pos> X<pos> Y<pos> Z<pos>
  float pos_a = 0.0;
  float pos_b = 0.0;
  float pos_c = 0.0;
  float pos_x = 0.0;
  float pos_y = 0.0;
  float pos_z = 0.0;
  char a_str, b_str, c_str, x_str, y_str, z_str;
  int n = sscanf(reply.c_str(), "%c%f %c%f %c%f %c%f %c%f %c%f", 
    &a_str, &pos_a, &b_str, &pos_b, &c_str, &pos_c, &x_str, &pos_x, &y_str, &pos_y, &z_str, &pos_z);

  if (n != 12)
  {
    // error
    err_code = pmac_CTRL_ERROR;
    this->m_error_string = "Unexpected PMAC reply for axes positions: " + reply;
    return err_code;        
  }

  //std::cout << "reply decrypt: pos_a = " << pos_a
  //  << " - pos b = " << pos_b << " - pos c = " << pos_c << std::endl;

  // parse list of axes to put positions in the same order
  for (size_t idx = 0; idx < ids.size(); idx++)
  {
    T_axis_id axis_id = ids[idx];
    char cs_str[4];
    sprintf(cs_str, "%d", axis_id.cs);

    // check axis validity
    if (this->m_axis_list.count(axis_id) == 0)
    {
      // error
      err_code = pmac_configuration_ERROR;
      this->m_error_string = "Undeclared axis: (" + std::string(cs_str) + std::string(", ")
        + axis_id.axis_name + std::string(")!");
      return err_code;
    }

    if (strncmp(axis_id.axis_name.c_str(), &a_str, 1) == 0)
    {
        // in omega case, inverse rotation direction
	    double a_pos = -1.0 * (double)pos_a;
        pos_list.push_back(a_pos);
    }
    else if (strncmp(axis_id.axis_name.c_str(), &b_str, 1) == 0)
    {
      pos_list.push_back((double)pos_b);
    }
    else if (strncmp(axis_id.axis_name.c_str(), &c_str, 1) == 0)
    {
      pos_list.push_back((double)pos_c);
    }
    else if (strncmp(axis_id.axis_name.c_str(), &x_str, 1) == 0)
    {
      pos_list.push_back((double)pos_x);
    }
    else if (strncmp(axis_id.axis_name.c_str(), &y_str, 1) == 0)
    {
      pos_list.push_back((double)pos_y);
    }
    else if (strncmp(axis_id.axis_name.c_str(), &z_str, 1) == 0)
    {
      pos_list.push_back((double)pos_z);
    }
    else
    {
      // error
      err_code = pmac_configuration_ERROR;
      this->m_error_string = "Unknown axis: (" + std::string(cs_str) + std::string(", ")
        + axis_id.axis_name + std::string(")!");
      return err_code;
    }
  }

   
#else

  std::string cmd;
  cmd = std::string("&") + yat::XString<unsigned int>::to_string(kSMARGON_CS_VALUE) 
    + std::string("p\n");
  float pos_a = 0.0;
  float pos_b = 0.0;
  float pos_c = 0.0;
  float pos_x = 0.0;
  float pos_y = 0.0;
  float pos_z = 0.0;
  char a_str, b_str, c_str, x_str, y_str, z_str;

  std::string reply = "A12.3 B0.55526 C-568.2 X.3969 Y-.325 Z14";
  int n = sscanf(reply.c_str(), "%c%f %c%f %c%f %c%f %c%f %c%f", 
    &a_str, &pos_a, &b_str, &pos_b, &c_str, &pos_c, &x_str, &pos_x, &y_str, &pos_y, &z_str, &pos_z);

  for (size_t idx = 0; idx < ids.size(); idx++)
  {
    T_axis_id axis_id = ids[idx];
    char cs_str[4];
    sprintf(cs_str, "%d", axis_id.cs);

    if (strncmp(axis_id.axis_name.c_str(), &a_str, 1) == 0)
    {
      pos_list.push_back(static_cast<double>(pos_a));
    }
    else if (strncmp(axis_id.axis_name.c_str(), &b_str, 1) == 0)
    {
      pos_list.push_back((double)pos_b);
    }
    else if (strncmp(axis_id.axis_name.c_str(), &c_str, 1) == 0)
    {
      pos_list.push_back((double)pos_c);
    }
    else if (strncmp(axis_id.axis_name.c_str(), &x_str, 1) == 0)
    {
      pos_list.push_back((double)pos_x);
    }
    else if (strncmp(axis_id.axis_name.c_str(), &y_str, 1) == 0)
    {
      pos_list.push_back((double)pos_y);
    }
    else if (strncmp(axis_id.axis_name.c_str(), &z_str, 1) == 0)
    {
      pos_list.push_back((double)pos_z);
    }
    else
    {
      // error
      err_code = pmac_configuration_ERROR;
      this->m_error_string = "Unknown axis: (" + std::string(cs_str) + std::string(", ")
        + axis_id.axis_name + std::string(")!");
      return err_code;
    }
  }

#endif

  return err_code;
}

#if !defined (_SIMULATION_)
// ============================================================================
// SGonPMAC::is_cs_declared
// ============================================================================
bool SGonPMAC::is_cs_declared(T_cs_id id)
{
  bool cs_found = false;
  // only one CS available for SMARGON
  if (kSMARGON_CS_VALUE == id)
    cs_found = true;

  return cs_found;
}
#endif


// ============================================================================
// SGonPMAC::set_cs_moving_mode
// ============================================================================
E_pmac_errno SGonPMAC::set_cs_moving_mode(T_cs_id id, E_moving_mode_t mode)
{
  std::cout << "SGonPMAC::set_cs_moving_mode() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

  // check mode value: only linear available for now
  if (mode != CS_MV_MODE_LINEAR)
  {
    m_error_string = "Bad moving mode value: only LINEAR mode available!";
    err_code = pmac_range_ERROR;
  }

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    // nothing to do, moving mode is linear
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    this->m_sim_cs_param[id].mv_mode = mode;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_cs_moving_mode
// ============================================================================
E_pmac_errno SGonPMAC::get_cs_moving_mode(T_cs_id id, E_moving_mode_t& mode)
{
  //std::cout << "SGonPMAC::get_cs_moving_mode() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    // moving mode = LINEAR
    mode = CS_MV_MODE_LINEAR;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    mode = this->m_sim_cs_param[id].mv_mode;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::start_motion_program
// ============================================================================
E_pmac_errno SGonPMAC::start_motion_program(T_cs_id id, int mp)
{
  std::cout << "SGonPMAC::start_motion_program() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    char mp_str[10];
    sprintf(mp_str, "%d", mp);

    // run program on CS: &<cs>b<prog>r\n
    std::string reply;
    std::string cmd;
    cmd = "&" + std::string(cs_str) + std::string("b") +  
      std::string(mp_str) + std::string("r\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    std::cout << "starting mprog: " << mp << std::endl;
    m_sim_mp_state = true;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::get_motion_program_state
// ============================================================================
E_pmac_errno SGonPMAC::get_motion_program_state(T_cs_id id, E_mp_state_t& st)
{
  //std::cout << "SGonPMAC::get_motion_program_state() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  st = MP_STATE_UNKNOWN;

  if (this->is_cs_declared(id))
  {
    std::string reply;
    std::string cmd;
    cmd = kPMAC_VAR_NAME_CS_COORD + std::string(cs_str) + kPMAC_VAR_NAME_CMD_SEP
      + kPMAC_VAR_NAME_CS_MP_ACTIVE + std::string(" ") + kPMAC_VAR_NAME_CS_COORD + std::string(cs_str) + kPMAC_VAR_NAME_CMD_SEP
      + kPMAC_VAR_NAME_CS_MP_RUNNING + std::string("\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;

    int int_active = 0;
    int int_running = 0;
    int n = sscanf(reply.c_str(), "%d\n%d", &int_active, &int_running);

    // check answer validity
    if (n < 2)
    {
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for mprog status: " + reply;
      return err_code;        
    }
    if ((int_active != 0) && (int_active != 1))
    {
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for mprog status: " + reply;
      return err_code;        
    }
    if ((int_running != 0) && (int_running != 1))
    {
      // error
      err_code = pmac_CTRL_ERROR;
      this->m_error_string = "Unexpected PMAC reply for mprog status: " + reply;
      return err_code;        
    }

    // compose MP status
    if (int_active == 0)
    {
      // no motion program launched, state = NULL
      st = MP_STATE_NULL;
    }
    else
    {
      // motion program running or suspended
      if (int_running == 0)
      {
        // motion program suspended
        st = MP_STATE_HOLD;
      }
      else
      {
        // motion program running
        st = MP_STATE_RUNNING;
      }
    }
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    st = MP_STATE_STDBY;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}


// ============================================================================
// SGonPMAC::pause_current_motion_program
// ============================================================================
E_pmac_errno SGonPMAC::pause_current_motion_program(T_cs_id id)
{
  std::cout << "SGonPMAC::pause_current_motion_program() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    // pause MP on CS: &1q\n
    std::string reply;
    std::string cmd;
    cmd = "&" + std::string(cs_str) + std::string("q\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    std::cout << "pausing current mprog" << std::endl;
    m_sim_mp_state = false;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

// ============================================================================
// SGonPMAC::resume_current_motion_program
// ============================================================================
E_pmac_errno SGonPMAC::resume_current_motion_program(T_cs_id id)
{
  std::cout << "SGonPMAC::resume_current_motion_program() entering..." << std::endl;
  E_pmac_errno err_code = pmac_NO_ERROR;
  char cs_str[4];
  sprintf(cs_str, "%d", id);

#if !defined (_SIMULATION_)  
  if (this->is_cs_declared(id))
  {
    // resume MP on CS: &1r\n
    std::string reply;
    std::string cmd;
    cmd = "&" + std::string(cs_str) + std::string("r\n");

    err_code = write_read(cmd, reply);
    if (err_code != pmac_NO_ERROR)
      return err_code;
  }
  else
  {
    // error
    err_code = pmac_configuration_ERROR;
    char cs_str[4];
    sprintf(cs_str, "%d", id);
    this->m_error_string = "Undeclared CS: (" + std::string(cs_str) + std::string(")!");
  }
#else
  if (this->m_sim_cs_param.count(id) != 0)
  {
    std::cout << "resuming current mprog" << std::endl;
  }
  else
  {
    std::string cs_str = std::string("(") + yat::XString<unsigned int>::to_string(id) + std::string(")");
    m_error_string = "CS " + cs_str + " not declared in driver!";
    err_code = pmac_configuration_ERROR;
  }
#endif

  return err_code;
}

#if !defined (_SIMULATION_)
// ============================================================================
// SGonPMAC::write_read
// ============================================================================
E_pmac_errno SGonPMAC::write_read(std::string cmd, std::string& reply)
{
  SSHDriverStatus ssh_status;
  size_t bytes = 0;
  char buff[5120] = "";
  E_pmac_errno err_code = pmac_NO_ERROR;

  CHECK_PMAC_CTRL;

  // check connection is open
  if (!m_is_connected)
  {
    // error
    err_code = pmac_libssh_ERROR;
    this->m_error_string = "SSH connection not open! Cannot send command: " + cmd;
    return err_code;
  }

  // send command on ssh connection
  ssh_status = m_sshdriver->write(cmd.c_str(), cmd.length(), &bytes, kPMAC_BOX_TIMEOUT);
  if (ssh_status != SSHDriverSuccess)
  {
    // error
    err_code = pmac_libssh_ERROR;
    char errno_c[10];
    sprintf(errno_c, "%d", ssh_status);

    this->m_error_string = "Lib ssh error occurred while requesting for controller status: " + std::string(errno_c);
    return err_code;
  }

  // read answer
  ssh_status = m_sshdriver->read(buff, 5120, &bytes, 0x06, kPMAC_BOX_TIMEOUT);
  if (ssh_status != SSHDriverSuccess)
  {
    // error
    err_code = pmac_libssh_ERROR;
    char errno_c[10];
    sprintf(errno_c, "%d", ssh_status);

    this->m_error_string = "Lib ssh error occurred while reading controller status: " + std::string(errno_c);
    return err_code;
  }

  // analyze answer
  reply = trim_right_copy(std::string(buff));
  int pmac_err_num = check_PowerPMAC_error(reply);
  if (pmac_err_num != 0)
  {
    err_code = pmac_CTRL_ERROR;
    char errno_c[10];
    sprintf(errno_c, "%d", pmac_err_num);
    this->m_error_string = "PMAC returns an error reading " + cmd + "'s reply: " + std::string(errno_c);
  }

  return err_code;
}
#endif

} // namespace sgonpmaclib
