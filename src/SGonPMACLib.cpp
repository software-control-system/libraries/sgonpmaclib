//=============================================================================
// SGonPMACLib.cpp
//=============================================================================
// abstraction.......SMARGON PMAC Application Programming Interface
// class.............SMARGON PMAC library API implementation
// original author...S. MINOLLI
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "SGonPMAC/SGonPMACLib.h"
#include "SGonPMAC/SGonPMAC.h"


namespace sgonpmaclib {

// ============================================================================
//- Check low level driver pointer:
#define CHECK_PMAC \
  if (!m_pmac_driver) \
  { \
     throw Exception(static_cast<const char*>("INTERNAL_ERROR"), \
                     static_cast<const char*>("Low level driver pointer is null!"), \
                     static_cast<const char*>("SGonPMAClib::CHECK_PMAC()")); \
  }
// ============================================================================

// ============================================================================
// SGonPMAClib::SGonPMAClib
// ============================================================================
SGonPMAClib::SGonPMAClib ()
{
  std::cout << "SGonPMAClib constructor entering..." << std::endl;
  m_pmac_driver = NULL;

  // Create TDC driver
  m_pmac_driver = new SGonPMAC();

  CHECK_PMAC;
}

// ============================================================================
// SGonPMAClib::~SGonPMAClib
// ============================================================================
SGonPMAClib::~SGonPMAClib ()
{
  std::cout << "SGonPMAClib destructor entering..." << std::endl;

  if (this->m_pmac_driver)
  {
    delete m_pmac_driver;
    this->m_pmac_driver = NULL;
  }
}

// ============================================================================
// SGonPMAClib::init
// ============================================================================
void SGonPMAClib::open(std::string ipAddress, unsigned int tcpPort)
  throw (Exception)
{
  std::cout << "SGonPMAClib initialization entering..." << std::endl;

  CHECK_PMAC;

  std::string user = "root";
  std::string passwd = "deltatau";

  if (ipAddress.empty())
  {
    std::string msg = "Empty IP address";
    throw Exception(static_cast<const char*>("CONFIGURATION_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::open()"));
  }

  if (tcpPort == 0)
  {
    std::string msg = "Null TCP port";
    throw Exception(static_cast<const char*>("CONFIGURATION_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::open()"));
  }

  E_pmac_errno err_code = this->m_pmac_driver->init_api(ipAddress, user, passwd, tcpPort);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::open()"));
  }
}

// ============================================================================
// SGonPMAClib::close
// ============================================================================
void SGonPMAClib::close()
  throw (Exception)
{
  std::cout << "SGonPMAClib close entering..." << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->close();
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::open()"));
  }
}

// ============================================================================
// SGonPMAClib::is_connection_opened
// ============================================================================
bool SGonPMAClib::is_connection_opened()
  throw (Exception)
{
  CHECK_PMAC;

  bool is_connected = false;
  E_pmac_errno err_code = this->m_pmac_driver->get_connection_state(is_connected);

  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::is_connection_opened()"));
  }

  return is_connected;
}

// ============================================================================
// SGonPMAClib::get_raw_status
// ============================================================================
T_ppmac_brick_status SGonPMAClib::get_raw_status()
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_raw_status entering..." << std::endl;

  CHECK_PMAC;

  T_ppmac_brick_status status;

  E_pmac_errno err_code = this->m_pmac_driver->get_raw_status(status);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_raw_status()"));
  }

  return status;
}


// ============================================================================
// SGonPMAClib::get_cpu_temperature
// ============================================================================
double SGonPMAClib::get_cpu_temperature()
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_raw_status entering..." << std::endl;

  CHECK_PMAC;
  double temp = 0.0;

  E_pmac_errno err_code = this->m_pmac_driver->get_cpu_temperature(temp);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cpu_temperature()"));
  }

  return temp;
}
	
// ============================================================================
// SGonPMAClib::get_cpu_running_time
// ============================================================================
double SGonPMAClib::get_cpu_running_time()
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cpu_running_time entering..." << std::endl;

  CHECK_PMAC;
  double cpu_time;

  E_pmac_errno err_code = this->m_pmac_driver->get_cpu_running_time(cpu_time);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cpu_running_time()"));
  }

  return cpu_time;
}

// ============================================================================
// SGonPMAClib::get_cpu_phase_usage
// ============================================================================
double SGonPMAClib::get_cpu_phase_usage()
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cpu_phase_usage entering..." << std::endl;

  CHECK_PMAC;
  double cpu_usage;

  E_pmac_errno err_code = this->m_pmac_driver->get_cpu_phase_usage(cpu_usage);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cpu_phase_usage()"));
  }

  return cpu_usage;
}

// ============================================================================
// SGonPMAClib::get_cpu_servo_usage
// ============================================================================
double SGonPMAClib::get_cpu_servo_usage()
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cpu_servo_usage entering..." << std::endl;

  CHECK_PMAC;
  double cpu_usage;

  E_pmac_errno err_code = this->m_pmac_driver->get_cpu_servo_usage(cpu_usage);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cpu_servo_usage()"));
  }

  return cpu_usage;
}

// ============================================================================
// SGonPMAClib::get_cpu_rti_usage
// ============================================================================
double SGonPMAClib::get_cpu_rti_usage()
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cpu_rti_usage entering..." << std::endl;

  CHECK_PMAC;
  double cpu_usage;

  E_pmac_errno err_code = this->m_pmac_driver->get_cpu_rti_usage(cpu_usage);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cpu_rti_usage()"));
  }

  return cpu_usage;
}

// ============================================================================
// SGonPMAClib::get_cpu_bg_usage
// ============================================================================
double SGonPMAClib::get_cpu_bg_usage()
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cpu_bg_usage entering..." << std::endl;

  CHECK_PMAC;
  double cpu_usage;

  E_pmac_errno err_code = this->m_pmac_driver->get_cpu_bg_usage(cpu_usage);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cpu_bg_usage()"));
  }

  return cpu_usage;
}

// ============================================================================
// SGonPMAClib::reset
// ============================================================================
void SGonPMAClib::reset()
  throw (Exception)
{
  std::cout << "SGonPMAClib reset entering..." << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->reset();
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::reset()"));
  }
}

// ============================================================================
// SGonPMAClib::reboot_os
// ============================================================================
void SGonPMAClib::reboot_os()
  throw (Exception)
{
  std::cout << "SGonPMAClib reboot_os entering..." << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->reboot();
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::reboot_os()"));
  }
}

// ============================================================================
// SGonPMAClib::abort
// ============================================================================
void SGonPMAClib::abort()
  throw (Exception)
{
  std::cout << "SGonPMAClib abort entering..." << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->abort();
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::abort()"));
  }
}

// ============================================================================
// SGonPMAClib::send_low_level_cmd
// ============================================================================
std::string SGonPMAClib::send_low_level_cmd(std::string cmd)
  throw (Exception)
{
  std::cout << "SGonPMAClib send_low_level_cmd entering..." << std::endl;
  std::cout << "cmd = " << cmd << std::endl;

  CHECK_PMAC;
  std::string cmd_result;

  E_pmac_errno err_code = this->m_pmac_driver->send_cmd(cmd, cmd_result);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::send_low_level_cmd()"));
  }

  return cmd_result;
}

// ============================================================================
// SGonPMAClib::get_firmware_vers
// ============================================================================
std::string SGonPMAClib::get_firmware_vers()
  throw (Exception)
{
  std::cout << "SGonPMAClib get_firmware_vers entering..." << std::endl;

  CHECK_PMAC;
  std::string vers;

  E_pmac_errno err_code = this->m_pmac_driver->get_firmware_rev(vers);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_firmware_vers()"));
  }

  return vers;
}

// ============================================================================
// SGonPMAClib::get_ucode_vers
// ============================================================================
std::string SGonPMAClib::get_ucode_vers()
  throw (Exception)
{
  std::cout << "SGonPMAClib get_ucode_vers entering..." << std::endl;

  CHECK_PMAC;
  std::string vers;

  E_pmac_errno err_code = this->m_pmac_driver->get_ucode_rev(vers);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_ucode_vers()"));
  }

  return vers;
}

// ============================================================================
// SGonPMAClib::init_axis
// ============================================================================
void SGonPMAClib::init_axis(T_axis_id id)
  throw (Exception)
{
  std::cout << "SGonPMAClib init_axis entering..." << std::endl;
  std::cout << "(cs, axis) = (" << id.cs << ", " << id.axis_name << ")" << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->init_axis(id);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::init_axis()"));
  }
}

// ============================================================================
// SGonPMAClib::undeclare_axis
// ============================================================================
void SGonPMAClib::undeclare_axis(T_axis_id id)
  throw (Exception)
{
  std::cout << "SGonPMAClib undeclare_axis entering..." << std::endl;
  std::cout << "(cs, axis) = (" << id.cs << ", " << id.axis_name << ")" << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->undeclare_axis(id);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::undeclare_axis()"));
  }
}

// ============================================================================
// SGonPMAClib::get_cs_status
// ============================================================================
T_cs_status SGonPMAClib::get_cs_status(T_cs_id id)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cs_status entering..." << std::endl;

  CHECK_PMAC;
  T_cs_status status;

  E_pmac_errno err_code = this->m_pmac_driver->get_cs_status(id, status);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cs_status()"));
  }

  return status;
}

// ============================================================================
// SGonPMAClib::get_cs_motors_status
// ============================================================================
std::string SGonPMAClib::get_cs_motors_status(T_cs_id id)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cs_motors_status entering..." << std::endl;

  CHECK_PMAC;

  char cs_str[4];
  sprintf(cs_str, "%d", id);
  std::string motors_status = "CS " + std::string(cs_str) + " motors status: UNKNOWN";

  E_pmac_errno err_code = this->m_pmac_driver->get_cs_motors_status(id, motors_status);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cs_motors_status()"));
  }

  return motors_status;
}

// ============================================================================
// SGonPMAClib::get_cs_ref_status
// ============================================================================
E_cs_referencing_status_t SGonPMAClib::get_cs_ref_status(T_cs_id id)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cs_ref_status entering..." << std::endl;

  CHECK_PMAC;

  E_cs_referencing_status_t ref_status = CS_REF_POS_UNKNOWN;

  E_pmac_errno err_code = this->m_pmac_driver->get_cs_ref_status(id, ref_status);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cs_ref_status()"));
  }

  return ref_status;
}

// ============================================================================
// SGonPMAClib::motor_on
// ============================================================================
void SGonPMAClib::motor_on(T_cs_id id)
  throw (Exception)
{
  std::cout << "SGonPMAClib motor_on entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->motor_on(id);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::motor_on()"));
  }
}

// ============================================================================
// SGonPMAClib::motor_off
// ============================================================================
void SGonPMAClib::motor_off(T_cs_id id)
  throw (Exception)
{
  std::cout << "SGonPMAClib motor_off entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->motor_off(id);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::motor_off()"));
  }
}

// ============================================================================
// SGonPMAClib::motor_omega_off
// ============================================================================
void SGonPMAClib::motor_omega_off(T_cs_id id)
  throw (Exception)
{
  std::cout << "SGonPMAClib motor_omega_off entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->motor_omega_off(id);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::motor_omega_off()"));
  }
}

// ============================================================================
// SGonPMAClib::motor_other_off
// ============================================================================
void SGonPMAClib::motor_other_off(T_cs_id id)
  throw (Exception)
{
  std::cout << "SGonPMAClib motor_other_off entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->motor_other_off(id);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::motor_other_off()"));
  }
}

// ============================================================================
// SGonPMAClib::axis_stop
// ============================================================================
void SGonPMAClib::axis_stop(T_cs_id id)
  throw (Exception)
{
  std::cout << "SGonPMAClib axis_stop entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->axis_stop(id);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::axis_stop()"));
  }
}

// ============================================================================
// SGonPMAClib::axis_init_ref_pos
// ============================================================================
void SGonPMAClib::axis_init_ref_pos(T_cs_id id)
  throw (Exception)
{
  std::cout << "SGonPMAClib axis_init_ref_pos entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;

  CHECK_PMAC;

  // start axis position initialization with stored init position & type
  E_pmac_errno err_code = this->m_pmac_driver->axis_init_ref_pos(id);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::axis_init_ref_pos()"));
  }
}

// ============================================================================
// SGonPMAClib::set_axis_position
// ============================================================================
void SGonPMAClib::set_axis_position(T_axis_id id, double position, bool apply)
  throw (Exception)
{
  std::cout << "SGonPMAClib set_axis_position entering..." << std::endl;
  std::cout << "(cs, axis) = (" << id.cs << ", " << id.axis_name << ")" << std::endl;
  std::cout << "position = " << position << " - apply = " << apply << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->set_axis_position(id, position, apply);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::set_axis_position()"));
  }
}

// ============================================================================
// SGonPMAClib::apply_axis_position
// ============================================================================
void SGonPMAClib::apply_axis_position(T_cs_id id)
  throw (Exception)
{
  std::cout << "SGonPMAClib apply_axis_position entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->apply_axis_position(id);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::apply_axis_position()"));
  }
}

// ============================================================================
// SGonPMAClib::set_axis_pos_offset
// ============================================================================
void SGonPMAClib::set_axis_pos_offset(T_axis_id id, double offset)
  throw (Exception)
{
  std::cout << "SGonPMAClib set_axis_pos_offset entering..." << std::endl;
  std::cout << "(cs, axis) = (" << id.cs << ", " << id.axis_name << ")" << std::endl;
  std::cout << "offset = " << offset << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->set_axis_pos_offset(id, offset);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::set_axis_pos_offset()"));
  }
}

// ============================================================================
// SGonPMAClib::get_axis_pos_offset
// ============================================================================
double SGonPMAClib::get_axis_pos_offset(T_axis_id id)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_axis_pos_offset entering..." << std::endl;

  double offset = 0.0;

  E_pmac_errno err_code = this->m_pmac_driver->get_axis_pos_offset(id, offset);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_axis_pos_offset()"));
  }

  return offset;
}

// ============================================================================
// SGonPMAClib::set_axis_compensation
// ============================================================================
void SGonPMAClib::set_axis_compensation(T_axis_id id, bool tof)
  throw (Exception)
{
  std::cout << "SGonPMAClib set_axis_compensation entering..." << std::endl;
  std::cout << "(cs, axis) = (" << id.cs << ", " << id.axis_name << ")" << std::endl;
  if (tof)
    std::cout << "state = ENABLED" << std::endl;
  else
    std::cout << "state = DISABLED" << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->set_axis_compensation(id, tof);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::set_axis_compensation()"));
  }
}

// ============================================================================
// SGonPMAClib::get_axis_compensation
// ============================================================================
bool SGonPMAClib::get_axis_compensation(T_axis_id id)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_axis_compensation entering..." << std::endl;

  bool enabled = false;

  E_pmac_errno err_code = this->m_pmac_driver->get_axis_compensation(id, enabled);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_axis_compensation()"));
  }

  return enabled;
}

// ============================================================================
// SGonPMAClib::get_cs_compensation_validity
// ============================================================================
bool SGonPMAClib::get_cs_compensation_validity(T_cs_id id)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cs_compensation_validity entering..." << std::endl;

  CHECK_PMAC;
  bool valid = false;

  E_pmac_errno err_code = this->m_pmac_driver->get_cs_compens_valid(id, valid);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cs_compensation_validity()"));
  }

  return valid;
}

// ============================================================================
// SGonPMAClib::set_cs_velocity
// ============================================================================
void SGonPMAClib::set_cs_velocity(T_cs_id id, double velocity)
  throw (Exception)
{
  std::cout << "SGonPMAClib set_cs_velocity entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;
  std::cout << "velocity: " << velocity << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->set_cs_velocity(id, velocity);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::set_cs_velocity()"));
  }
}

// ============================================================================
// SGonPMAClib::get_cs_velocity
// ============================================================================
double SGonPMAClib::get_cs_velocity(T_cs_id id)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cs_velocity entering..." << std::endl;

  CHECK_PMAC;
  double vel = 0.0;

  E_pmac_errno err_code = this->m_pmac_driver->get_cs_velocity(id, vel);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cs_velocity()"));
  }

  return vel;
}

// ============================================================================
// SGonPMAClib::set_cs_acceleration_time
// ============================================================================
void SGonPMAClib::set_cs_acceleration_time(T_cs_id id, double acceleration)
  throw (Exception)
{
  std::cout << "SGonPMAClib set_cs_acceleration_time entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;
  std::cout << "acceleration time: " << acceleration << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->set_cs_acceleration(id, acceleration);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::set_cs_acceleration_time()"));
  }
}

// ============================================================================
// SGonPMAClib::get_cs_acceleration_time
// ============================================================================
double SGonPMAClib::get_cs_acceleration_time(T_cs_id id)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cs_acceleration_time entering..." << std::endl;

  CHECK_PMAC;
  double accel = 0.0;

  E_pmac_errno err_code = this->m_pmac_driver->get_cs_acceleration(id, accel);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cs_acceleration_time()"));
  }

  return accel;
}

// ============================================================================
// SGonPMAClib::set_cs_deceleration_time
// ============================================================================
void SGonPMAClib::set_cs_deceleration_time(T_cs_id id, double deceleration)
  throw (Exception)
{
  std::cout << "SGonPMAClib set_cs_deceleration_time entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;
  std::cout << "deceleration time: " << deceleration << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->set_cs_deceleration(id, deceleration);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::set_cs_deceleration_time()"));
  }
}

// ============================================================================
// SGonPMAClib::get_cs_deceleration_time
// ============================================================================
double SGonPMAClib::get_cs_deceleration_time(T_cs_id id)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cs_deceleration_time entering..." << std::endl;

  CHECK_PMAC;
  double decel = 0.0;

  E_pmac_errno err_code = this->m_pmac_driver->get_cs_deceleration(id, decel);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cs_deceleration_time()"));
  }

  return decel;
}

// ============================================================================
// SGonPMAClib::set_cs_scurve_time
// ============================================================================
void SGonPMAClib::set_cs_scurve_time(T_cs_id id, double scurve)
  throw (Exception)
{
  std::cout << "SGonPMAClib set_cs_scurve_time entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;
  std::cout << "scurve time: " << scurve << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->set_cs_scurve(id, scurve);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::set_cs_scurve_time()"));
  }
}

// ============================================================================
// SGonPMAClib::get_cs_scurve_time
// ============================================================================
double SGonPMAClib::get_cs_scurve_time(T_cs_id id)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cs_scurve_time entering..." << std::endl;

  CHECK_PMAC;
  double scurve = 0.0;

  E_pmac_errno err_code = this->m_pmac_driver->get_cs_scurve(id, scurve);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cs_scurve_time()"));
  }

  return scurve;
}

// ============================================================================
// SGonPMAClib::get_axes_positions
// ============================================================================
std::vector<double> SGonPMAClib::get_axes_positions(std::vector<T_axis_id> ids)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_axes_positions entering..." << std::endl;

  CHECK_PMAC;
  std::vector<double> pos_list;

  E_pmac_errno err_code = this->m_pmac_driver->get_axes_positions(ids, pos_list);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_axes_positions()"));
  }

  return pos_list;
}

// ============================================================================
// SGonPMAClib::set_cs_moving_mode
// ============================================================================
void SGonPMAClib::set_cs_moving_mode(T_cs_id id, E_moving_mode_t mode)
  throw (Exception)
{
  std::cout << "SGonPMAClib set_cs_moving_mode entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;
  std::cout << "mode: " << (unsigned int)mode << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->set_cs_moving_mode(id, mode);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::set_cs_moving_mode()"));
  }
}

// ============================================================================
// SGonPMAClib::get_cs_moving_mode
// ============================================================================
E_moving_mode_t SGonPMAClib::get_cs_moving_mode(T_cs_id id)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_cs_moving_mode entering..." << std::endl;

  CHECK_PMAC;
  E_moving_mode_t mode = CS_MV_MODE_LINEAR;

  E_pmac_errno err_code = this->m_pmac_driver->get_cs_moving_mode(id, mode);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_cs_moving_mode()"));
  }

  return mode;
}

// ============================================================================
// SGonPMAClib::start_motion_program
// ============================================================================
void SGonPMAClib::start_motion_program(T_cs_id id, int mp)
  throw (Exception)
{
  std::cout << "SGonPMAClib start_motion_program entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;
  std::cout << "motion program: " << mp << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->start_motion_program(id, mp);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::start_motion_program()"));
  }
}

// ============================================================================
// SGonPMAClib::get_motion_program_state
// ============================================================================
E_mp_state_t SGonPMAClib::get_motion_program_state(T_cs_id id)
  throw (Exception)
{
  //std::cout << "SGonPMAClib get_motion_program_state entering..." << std::endl;
  //std::cout << "(cs) = (" << id << ")" << std::endl;

  CHECK_PMAC;

  E_mp_state_t st;

  E_pmac_errno err_code = this->m_pmac_driver->get_motion_program_state(id, st);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::get_motion_program_state()"));
  }

  return st;
}

// ============================================================================
// SGonPMAClib::pause_current_motion_program
// ============================================================================
void SGonPMAClib::pause_current_motion_program(T_cs_id id)
  throw (Exception)
{
  std::cout << "SGonPMAClib pause_current_motion_program entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->pause_current_motion_program(id);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::pause_current_motion_program()"));
  }
}

// ============================================================================
// SGonPMAClib::resume_current_motion_program
// ============================================================================
void SGonPMAClib::resume_current_motion_program(T_cs_id id)
  throw (Exception)
{
  std::cout << "SGonPMAClib resume_current_motion_program entering..." << std::endl;
  std::cout << "(cs) = (" << id << ")" << std::endl;

  CHECK_PMAC;

  E_pmac_errno err_code = this->m_pmac_driver->resume_current_motion_program(id);
  if (err_code != pmac_NO_ERROR)
  {
    std::string msg = this->m_pmac_driver->get_error_string();
    throw Exception(static_cast<const char*>("DRIVER_ERROR"),
                    static_cast<const char*>(msg.c_str()),
                    static_cast<const char*>("SGonPMAClib::resume_current_motion_program()"));
  }
}

} // namespace sgonpmaclib


