cmake_minimum_required(VERSION 3.15)
project(sgonpmaclib)

option(BUILD_SHARED_LIBS "Build using shared libraries" ON)

find_package(yat CONFIG REQUIRED)
find_package(ssh2 CONFIG REQUIRED)

add_compile_definitions(
    PROJECT_NAME=${PROJECT_NAME}
    PROJECT_VERSION=${PROJECT_VERSION}
)

file(GLOB_RECURSE sources
    src/*.cpp
)

set(includedirs 
    src
    include
)

add_library(sgonpmaclib ${sources})
target_include_directories(sgonpmaclib PRIVATE ${includedirs})
target_link_libraries(sgonpmaclib PRIVATE yat::yat)
target_link_libraries(sgonpmaclib PRIVATE ssh2::ssh2)

if (MAJOR_VERSION)
    set_target_properties(sgonpmaclib PROPERTIES VERSION ${PROJECT_VERSION} SOVERSION ${MAJOR_VERSION})
endif()

install(DIRECTORY ${CMAKE_SOURCE_DIR}/include/ DESTINATION include
    FILES_MATCHING PATTERN "*/*"
)
install(DIRECTORY ${CMAKE_SOURCE_DIR}/src/ DESTINATION include
    FILES_MATCHING PATTERN "*/*.h"
)

install(TARGETS sgonpmaclib LIBRARY DESTINATION ${LIB_INSTALL_DIR})
