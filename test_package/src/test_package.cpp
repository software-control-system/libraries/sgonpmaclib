#include <iostream>

#include <SGonPMAC/SGonPMAC.h>

int main()
{
    sgonpmaclib::SGonPMAC p = sgonpmaclib::SGonPMAC();
    double temp;
    p.get_cpu_temperature(temp);
    std::cout << temp << '\n';

    return 0;
}
