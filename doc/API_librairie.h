//=============================================================================
// SGonPMACLib.h
//=============================================================================
// abstraction.......SMARGON PMAC Application Programming Interface
// class.............SMARGON PMAC library API specification
// original author...S. MINOLLI
//=============================================================================

#ifndef _SMARGON_PMAC_LIB_H_
#define _SMARGON_PMAC_LIB_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "SGonPMAC.h"
#include "PMACException.h"

// ============================================================================
// API DEFINITION
//
// This API provides access to SMARGON controller and axes functions.
// The axis identifier managed by a library client is: Coordinate system, axis name
// i.e. (CS, name).
// The SGonPMAC library implements the mapping between the (CS, name) identifier and 
// the controller data structure in which the axis data is identified by a unique index.
// ============================================================================

namespace sgonpmaclib {

class SGonPMAC;

//***********************************************************************************
// SMARGON PMAC API
//***********************************************************************************
class SGonPMAClib
{

public:
  // Constructor.
  SGonPMAClib();

  // Destructor.
  virtual ~SGonPMAClib();

  //---------------------------------------------------------------------------
  // Controller management
  //---------------------------------------------------------------------------
  
  // Initializes the driver's connection.
  // The user and password for the ssh session are set by the library.
  // This function opens a ssh session with the controller.
  void open(std::string ipAddress, unsigned int tcpPort)
    throw (Exception);

  // Finalize the driver's connection.
  // This function closes the ssh session with the controller.
  void close()
    throw (Exception);

  // Gets the controller's connection state (ok/nok).
  // @return Connection opened (true/false)
  bool is_connection_opened()
    throw (Exception);

  // Gets the controller status word.
  // @return PPMAC controller current status (binary word)
  T_ppmac_brick_status get_raw_status()
    throw (Exception);
	
  // Gets the CPU temperature.
  // @return CPU temperature in Celsius degrees
  double get_cpu_temperature()
    throw (Exception);
	
  // Gets CPU running time from power-on to present, in seconds.
  // @return Running time (s). 
  double get_cpu_running_time()
    throw (Exception);

  // Gets CPU usage for phase task calculation.
  // @return Phase tasks usage (%). 
  double get_cpu_phase_usage()
    throw (Exception);

  // Gets CPU usage for servo task calculation.
  // @return Servo tasks usage (%). 
  double get_cpu_servo_usage()
    throw (Exception);

  // Gets CPU usage for real-time interrupt task calculation.
  // @return RTI tasks usage (%). 
  double get_cpu_rti_usage()
    throw (Exception);

  // Gets CPU usage for background task scans.
  // @return BG tasks usage (%). 
  double get_cpu_bg_usage()
    throw (Exception);

  // Resets the controller.
  // Does not break ssh connection.
  void reset()
    throw (Exception);

  // Reboots the controller operating system.
  void reboot_os()
    throw (Exception);
	
  // Stops all axes movements and plc programs.
  void abort()
    throw (Exception);
	
  // Executes a low level command.
  // @param cmd The low level command
  // @return Command reply.
  std::string send_low_level_cmd(std::string cmd)
    throw (Exception);
	
  // Gets firmware version.
  // @return Firmware version
  std::string get_firmware_vers()
    throw (Exception);

  // Gets micro code version.
  // @return Micro code version
  std::string get_ucode_vers()
    throw (Exception);

  //---------------------------------------------------------------------------
  //- Axis initilization
  //---------------------------------------------------------------------------

  // Initializes the axis identified with CS number and axis name in CS.
  // The library asks the associated data index to the controller.
  // @param id Axis id (cs, axis name).
  void init_axis(T_axis_id id)
    throw (Exception);

  // Delete axis reference from library internal list.
  // The axis is still declared in controller.
  // @param id Axis id (cs, axis name).
  void undeclare_axis(T_axis_id id)
    throw (Exception);

  // Gets current CS status.
  // @param id CS id.
  // @return CS status.
  T_cs_status get_cs_status(T_cs_id id)
    throw (Exception);

  // Gets motor status.
  // @param id CS id.
  // @return CS motors' status as string.
  std::string get_cs_motors_status(T_cs_id id)
    throw (Exception);


  //---------------------------------------------------------------------------
  // Axis actions
  //---------------------------------------------------------------------------

  // Powers on the underlying motor.
  // @param id CS id.
  void motor_on(T_cs_id id)
    throw (Exception);

  // Powers off the underlying motor.
  // @param id CS id.
  void motor_off(T_cs_id id)
    throw (Exception);

  // SPECIFIC SMARGON
  // Powers off the OMEGA axis underlying motor.
  // @param id CS id.
  void motor_omega_off(T_cs_id id)
    throw (Exception);

  // SPECIFIC SMARGON
  // Powers off all axes (except OMEGA) underlying motors.
  // @param id CS id.
  void motor_other_off(T_cs_id id)
    throw (Exception);

  // Stop current axis movement.
  // @param id CS id.
  void axis_stop(T_cs_id id)
    throw (Exception);

  // Starts position referencing.
  // @param id CS id.
  void axis_init_ref_pos(T_cs_id cs)
    throw (Exception);

  // Gets position referencing status.
  // @param id CS id.
  // @return Referencing status.
  E_cs_referencing_status_t get_cs_ref_status(T_cs_id cs)
    throw (Exception);


  //---------------------------------------------------------------------------
  // Axis attributes
  //---------------------------------------------------------------------------

  // Moves axis to specified absolute position, in mdeg (circular axis) or �m (linear axis).
  // @param id Axis id (cs, axis name).
  // @param position Axis absolute position setpoint in in mdeg (circular axis) or �m (linear axis).
  // @param apply True to set and apply position setpoint, false to only set position setpoint.
  void set_axis_position(T_axis_id id, double position, bool apply = true)
    throw (Exception);

  // Apply axis position setpoints in specified CS.
  // @param id CS id.
  void apply_axis_position(T_cs_id id)
    throw (Exception);

  // Sets axis position offset, in �m (linear axis)
  // @param id Axis id (cs, axis name).
  // @param offset Axis position offset in �m (linear axis).
  void set_axis_pos_offset(T_axis_id id, double offset)
    throw (Exception);

  // Gets current axis position offset, in �m (linear axis).
  // @param id Axis id (cs, axis name).
  // @return Current axis position offset in �m (linear axis).
  double get_axis_pos_offset(T_axis_id id)
    throw (Exception);

  // Sets axis velocity, in mdeg/s (circular axis) or �m/s (linear axis).
  // @param id CS id.
  // @param velocity Axis velocity setpoint in mdeg/s (circular axis) or �m/s (linear axis).
  void set_cs_velocity(T_cs_id id, double velocity)
    throw (Exception);

  // Gets current axis velocity, in mdeg/s (circular axis) or �m/s (linear axis).
  // @param id CS id.
  // @return Current axis velocity, in mdeg/s (circular axis) or �m/s (linear axis).
  double get_cs_velocity(T_cs_id id)
    throw (Exception);

  // Sets axis acceleration time, in ms.
  // @param id CS id.
  // @param acceleration Axis acceleration time setpoint in ms.
  void set_cs_acceleration_time(T_cs_id id, double acceleration)
    throw (Exception);

  // Gets current axis acceleration time, in ms.
  // @param id CS id.
  // @return Current axis acceleration time, in ms.
  double get_cs_acceleration_time(T_cs_id id)
    throw (Exception);

  // Sets axis deceleration time, in ms.
  // @param id CS id.
  // @param deceleration Axis deceleration time setpoint in ms.
  void set_cs_deceleration_time(T_cs_id id, double deceleration)
    throw (Exception);

  // Gets current axis deceleration time, in ms.
  // @param id CS id.
  // @return Current axis deceleration time, in ms.
  double get_cs_deceleration_time(T_cs_id id)
    throw (Exception);

  // Sets axis S-curve acceleration time, in ms.
  // @param id CS id.
  // @param scurve Axis S-curve acceleration time setpoint in ms.
  void set_cs_scurve_time(T_cs_id id, double scurve)
    throw (Exception);

  // Gets current axis S-curve acceleration time, in ms.
  // @param id CS id.
  // @return Current axis S-curve acceleration time, in ms.
  double get_cs_scurve_time(T_cs_id id)
    throw (Exception);

  // Sets axis moving mode.
  // @param id CS id.
  // @param mode CS moving mode.
  void set_cs_moving_mode(T_cs_id id, E_moving_mode_t mode)
    throw (Exception);

  // Gets current axis moving mode.
  // @param id CS id.
  // @return Current axis moving mode.
  E_moving_mode_t get_cs_moving_mode(T_cs_id id)
    throw (Exception);

  // Multi-axes management:

  // Gets current position of a list of axes.
  // @param ids Axis id (cs, axis name) list.
  // @return Position list (in axis list order), in mdeg/s (circular axis) or �m/s (linear axis).
  std::vector<double> get_axes_positions(std::vector<T_axis_id> ids)
    throw (Exception);


  //---------------------------------------------------------------------------
  // Motion programs 
  //---------------------------------------------------------------------------

  // Starts a motion program for the specified CS.
  // @param id CS id.
  // @param mp Motion program number.
  void start_motion_program(T_cs_id id, int mp)
    throw (Exception);

  // Gets the current motion program state for specified CS.
  // @param id CS id.
  // @return Motion program state.
  E_mp_state_t get_motion_program_state(T_cs_id id)
    throw (Exception);

  // Pauses the current motion program running for specified CS.
  // @param id CS id.
  void pause_current_motion_program(T_cs_id id)
    throw (Exception);

  // Resumes the current motion program running for specified CS.
  // @param id CS id.
  void resume_current_motion_program(T_cs_id id)
    throw (Exception);


  //---------------------------------------------------------------------------
  // Compensation tables
  //---------------------------------------------------------------------------

  // Enables/disables axis compensation table.
  // @param id Axis id.
  // @param tof true = enables, false = disables.
  void set_axis_compensation(T_axis_id id, bool tof)
    throw (Exception);

  // Gets the current compensation state for specified axis.
  // @param id Axis id.
  // @return Compensation state.
  bool get_axis_compensation(T_axis_id id)
    throw (Exception);

  // Gets the current compensation data validity for specified CS.
  // @param id CS id.
  // @return Compensation data validity: true = valid, false = not valid.
  bool get_cs_compensation_validity(T_cs_id id)
    throw (Exception);


private:

  // Low level driver access
  SGonPMAC * m_pmac_driver;

};

} // namespace sgonpmaclib

#endif // _SMARGON_PMAC_LIB_H_
