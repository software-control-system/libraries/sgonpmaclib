from conan import ConanFile

class sgonpmaclibRecipe(ConanFile):
    name = "sgonpmaclib"
    version = "1.0.6"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"
    
    license = "GPL-2"
    author = "Minolli"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/libraries/sgonpmaclib"
    description = "SGonPMACLib library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**", "include/**"

    def requirements(self):
        self.requires("yat/[>=1.0]@soleil/stable")
        self.requires("ssh2/1.4.3@soleil/stable")

    def package_info(self):
        self.cpp_info.libs = ["sgonpmaclib"]
        self.cpp_info.system_libs.extend(["rt"])
