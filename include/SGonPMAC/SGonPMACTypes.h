//=============================================================================
// SGonPMACTypes.h
//=============================================================================
// abstraction.......Basic types for SMARGON PMAC library
// class.............Basic types
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _SGON_TYPES_H_
#define _SGON_TYPES_H_


//=============================================================================
// DEPENDENCIES
//=============================================================================
#if (defined WIN32 || defined _WIN32)
#include <yat/CommonHeader.h> // to avoid WARNING 4290 windows
#endif


namespace sgonpmaclib
{

//- Binary word for PowerPMAC Brick status 
struct T_ppmac_brick_status {
  // Fatal errors:
  unsigned int WDTFault0 : 1; // Software watchdog fault (background failure)
  unsigned int WDTFault1 : 1; // Software watchdog fault (interrupt failure)
  unsigned int PwrOnFault : 1; // Power-on/reset load fault (OR of bits 3�6)
  unsigned int ProjectLoadErr : 1; // Project load error
  unsigned int ConfigLoadErr : 1; // Saved configuration load error
  unsigned int HWChangeErr : 1; // Hardware change detected since save
  unsigned int FileConfigErr : 1; // System file configuration error
  unsigned int Default : 1; // Factory default configuration (by cmd or error)
  
  unsigned int NoClocks : 1; // No system clocks found
  unsigned int AbortAll : 1; // Abort all requiered?
  unsigned int NotUsed10 : 1; // Reserved
  unsigned int NotUsed11 : 1; // Reserved
  unsigned int NotUsed12 : 1; // Reserved
  unsigned int FwdKinErr : 1; // Forward kinematic error
  unsigned int InvKinErr : 1; // Inv kinematic error
  unsigned int CalcCompErr : 1; // Calc comp error

  unsigned int BusOverVoltage : 1; // DC bus overvoltage fault flag
  unsigned int OverTemp : 1; // Power board overtemperature flag

  // Warnings:
  unsigned int BusUnderVoltage : 1; // DC bus undervoltage warning flag
  unsigned int NotUsed19 : 1; // Reserved
  unsigned int NotUsed20 : 1; // Reserved
  unsigned int NotUsed21 : 1; // Reserved
  unsigned int NotUsed22 : 1; // Reserved
  unsigned int NotUsed23 : 1; // Reserved
  
  unsigned int NotUsed24 : 1; // Reserved
  unsigned int NotUsed25 : 1; // Reserved
  unsigned int NotUsed26 : 1; // Reserved
  unsigned int NotUsed27 : 1; // Reserved
  unsigned int NotUsed28 : 1; // Reserved
  unsigned int NotUsed29 : 1; // Reserved
  unsigned int NotUsed30 : 1; // Reserved
  unsigned int NotUsed31 : 1; // Reserved
};

//- CS identifier
typedef unsigned int T_cs_id;

//- Axis identifier
typedef struct T_axis_id
{
  T_cs_id cs;       // Coordinate system number (1, 2, 10, ...)
  std::string axis_name; // Name of the axis in this CS (AA, BB, X, Y, Z, I, J, ...)

  //- default constructor --
  T_axis_id ()
    : cs(0),
      axis_name("")
  {
  }

  //- destructor -----------
  ~T_axis_id ()
  {
  }

  //- copy constructor -----
  T_axis_id (const T_axis_id& src)
  {
    *this = src;
  }

  //- operator= --------------
  const T_axis_id & operator= (const T_axis_id& src)
  {
      if (this == & src) 
        return *this;

      this->cs = src.cs;
      this->axis_name = src.axis_name;

      return *this;
  }

  //- operator< --------------
  bool operator< (const T_axis_id& src) const
  {
    bool result;
    if (this->cs == src.cs)
    {
      result = (this->axis_name < src.axis_name);
    }
    else
    {
      result = (this->cs < src.cs);
    }
    
    return result;
  }

  //- operator== --------------
  bool operator== (const T_axis_id& src) const
  {
    return ((this->cs == src.cs) && (this->axis_name == src.axis_name));
  }

} T_axis_id;


//- Binary word for CS status
struct T_cs_status {
  unsigned int SyncAssignError : 1; // Sync assignment buffer error
  unsigned int BufferError : 1; // Buffer error
  unsigned int LinToPvtError : 1; // Linear-to-PVT mode error
  unsigned int PvtError : 1; // PVT mode error
  unsigned int RunTimeError : 1; // Run time error
  unsigned int SoftLimit : 1; // Stopped on software position limit
  unsigned int RadiusError : 1; // X/Y/Z-axis circle radius error
  unsigned int RadiusError2 : 1; // XX/YY/ZZ-axis circle radius error

  unsigned int TimersEnabled : 1; // Timers enabled
  unsigned int BlockRequest : 1; // Block request flag set
  unsigned int NotUsed1 : 1; // reserved
  unsigned int InPos : 1; // In position
  unsigned int AmpEna : 1; // Amplifier enabled
  unsigned int ClosedLoop : 1; // Closed-loop mode
  unsigned int DesVelZero : 1; // Desired velocity zero
  unsigned int HomeComplete : 1; // Home complete

  unsigned int TimerEnabled : 1; // Move timer enabled
  unsigned int NotUsed2 : 1; // reserved
  unsigned int EncLoss : 1; // Sensor loss error
  unsigned int AmpWarn : 1; // Amp warning
  unsigned int TriggerNotFound : 1; // Trigger not found
  unsigned int I2tFault : 1; // Integrated current (I2T) fault
  unsigned int SoftPlusLimit : 1; // Software positive limit set
  unsigned int SoftMinusLimit : 1; // Software negative limit set

  unsigned int AmpFault : 1; // Amplifier fault
  unsigned int LimitStop : 1; // Stopped on hardware limit
  unsigned int FeFatal : 1; // Fatal following error
  unsigned int FeWarn : 1; // Warning following error
  unsigned int PlusLimit : 1; // Hardware positive limit set
  unsigned int MinusLimit : 1; // Hardware negative limit set
  unsigned int HomeInProgress : 1; // Home search move in progress
  unsigned int TriggerMove : 1; // Trigger search move in progress
};

//- CS position referencing status
typedef enum
{
  CS_REF_POS_UNKNOWN = 0,  // unknown
  CS_REF_POS_UNREF,        // unreferenced
  CS_REF_POS_RUNNING,      // in progress
  CS_REF_POS_OK,           // referenced successfully
  CS_REF_POS_KO            // error occurred
} E_cs_referencing_status_t;

//- Axis moving mode
typedef enum
{
  CS_MV_MODE_RAPID = 0, // each axis moves at configured velocity, all axes don't finish at the same time
  CS_MV_MODE_LINEAR,    // all axes move all together during a specified time at a global velocity
  CS_MV_MODE_SPLINE,    // continuous velocity and acceleration
  CS_MV_MODE_PVT,       // 
  CS_MV_MODE_CIRCLE     // 
} E_moving_mode_t;

//- Motion Program state
typedef enum
{
  MP_STATE_STDBY = 0,     // Motion program is STANDBY
  MP_STATE_FAULT,         // Motion program in FAULT
  MP_STATE_RUNNING,       // Motion program is RUNNING
  MP_STATE_STEP_RUNNING,  // Motion program is SINGLE STEP
  MP_STATE_HOLD,          // Motion program is HOLD (paused)
  MP_STATE_NULL,          // Motion program is NULL
  MP_STATE_ACTIVE,        // Motion program is ACTIVE
  MP_STATE_UNKNOWN        // Motion program is UNKNOWN
} E_mp_state_t;

} //- namespace sgonpmaclib

#endif //- _SGON_TYPES_H_

