//=============================================================================
// PMACException.h
//=============================================================================
// abstraction.......SMARGON PMAC Application Programming Interface
// class.............SMARGON PMAC Error & Exception specification
// original author...S. MINOLLI
//=============================================================================

#ifndef _PMAC_EXCEPTION_H_
#define _PMAC_EXCEPTION_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>
#include <vector>

namespace sgonpmaclib {

// ============================================================================
// PowerPMAC error severities 
// ============================================================================
typedef enum {
  WARN, 
  ERR, 
  PANIC
} Severity;

// ============================================================================
// The PowerPMAC Error abstraction base class.  
//
// Contains 5 fields:
// � reason
// � description
// � origin
// � error code
// � severity
// ============================================================================
class Error
{
public:

  // Constructor. 
  Error ();

  // Constructor with parameters.
  Error (const char *_reason,
				 const char *_desc,
				 const char *_origin,
	       int _code = -1, 
	       sgonpmaclib::Severity _severity = sgonpmaclib::ERR);
  

  // Constructor with parameters.
  Error (const std::string& _reason,
				 const std::string& _desc,
				 const std::string& _origin, 
	       int _code = -1, 
	       sgonpmaclib::Severity _severity = sgonpmaclib::ERR);

  // Copy constructor.
  Error (const Error& _src);

  // Destructor.
  virtual ~Error ();

  // Operator=
  Error& operator= (const Error& _src);

  // Error details: reason
  std::string reason;

  // Error details: description
  std::string desc;

  // Error details: origin
  std::string origin;

  // Error details: code
  int code;

  // Error details: severity
  sgonpmaclib::Severity severity;
};

// ============================================================================
// The PowerPMAC error list.	
// ============================================================================
typedef std::vector<Error> ErrorList;

// ============================================================================
// The PowerPMAC Exception abstraction base class.  
//  
// Contains a list of PowerPMAC Errors.
// 
// ============================================================================
class Exception
{
public:

  // Constructor.
  Exception ();

  // Constructor with parameters.
  Exception (const char *_reason,
					   const char *_desc,
					   const char *_origin,
	           int _code = -1, 
	           sgonpmaclib::Severity _severity = sgonpmaclib::ERR);
  
  // Constructor with parameters.
  Exception (const std::string& _reason,
					   const std::string& _desc,
					   const std::string& _origin, 
	           int _code = -1, 
	           sgonpmaclib::Severity _severity = sgonpmaclib::ERR);

  // Constructor from Error class.
  Exception (const Error& error);


  // Copy constructor.
  Exception (const Exception& _src);

  // Operator=
  Exception& operator= (const Exception& _src); 

  // Destructor.
  virtual ~Exception ();

  // Pushes the specified error into the errors list.
  void push_error (const char *_reason,
					         const char *_desc,
						       const char *_origin, 
		               int _code = -1, 
		               sgonpmaclib::Severity _severity = sgonpmaclib::ERR);

  // Pushes the specified error into the errors list.
  void push_error (const std::string& _reason,
                   const std::string& _desc,
                   const std::string& _origin, 
                   int _code = -1, 
                   sgonpmaclib::Severity _severity = sgonpmaclib::ERR);

  // Pushes the specified error into the errors list.
  void push_error (const Error& _error);

  // The error list.
  ErrorList errors;
};

} // namespace sgonpmaclib

#endif // _PMAC_EXCEPTION_H_

