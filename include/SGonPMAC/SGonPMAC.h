//=============================================================================
// SGonPMAC.h
//=============================================================================
// abstraction.......Power PMAC Application Programming Interface
// class.............PowerPMAC library API specification
// original author...S. MINOLLI
//=============================================================================

#ifndef _POWER_PMAC_H_
#define _POWER_PMAC_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <sstream>
#include "PMACTypesAndConsts.h"
#include "libssh2Driver.h"

namespace sgonpmaclib {


// ============================================================================
// API DEFINITION
// ============================================================================

class SGonPMAC
{

public:
  // Constructor.
  SGonPMAC();

  // Destructor.
  virtual ~SGonPMAC();

  // API initialization
  E_pmac_errno init_api(std::string ipAddress, std::string user, 
    std::string passwd, unsigned int tcpPort);

  // API finalization
  E_pmac_errno close();

  // gets pmac connection state (on/off)
  E_pmac_errno get_connection_state(bool& state);

  // gets pmac box row status
  E_pmac_errno get_raw_status(T_ppmac_brick_status& status);

  // gets cpu temperature
  E_pmac_errno get_cpu_temperature(double& temp);

  // gets cpu time
  E_pmac_errno get_cpu_running_time(double& cpu_time);

  // gets cpu usage (%)
  E_pmac_errno get_cpu_phase_usage(double& cpu_usage);
  E_pmac_errno get_cpu_servo_usage(double& cpu_usage);
  E_pmac_errno get_cpu_rti_usage(double& cpu_usage);
  E_pmac_errno get_cpu_bg_usage(double& cpu_usage);

  // resets pmac box
  E_pmac_errno reset();

  // reboots pmac box
  E_pmac_errno reboot();

  // aborts all CS movements
  E_pmac_errno abort();

  // sends low level command
  E_pmac_errno send_cmd(std::string cmd, std::string& cmd_result);

  // gets firmware version
  E_pmac_errno get_firmware_rev(std::string& vers);

  // gets ucode version
  E_pmac_errno get_ucode_rev(std::string& vers);

  // initializes axis
  E_pmac_errno init_axis(T_axis_id id);

  // undeclares axis
  E_pmac_errno undeclare_axis(T_axis_id id);

  // gets cs status
  E_pmac_errno get_cs_status(T_cs_id id, T_cs_status& status);

  // gets cs motors' status
  E_pmac_errno get_cs_motors_status(T_cs_id id, std::string& status);

  // gets cs positišon referencing status
  E_pmac_errno get_cs_ref_status(T_cs_id id, E_cs_referencing_status_t& status);
  
  // motors on/off/stop
  E_pmac_errno motor_on(T_cs_id id);
  E_pmac_errno motor_off(T_cs_id id);
  E_pmac_errno motor_omega_off(T_cs_id id);
  E_pmac_errno motor_other_off(T_cs_id id);

  // CS stop/init
  E_pmac_errno axis_stop(T_cs_id id);
  E_pmac_errno axis_init_ref_pos(T_cs_id id);
  
  // get/set axis position
  E_pmac_errno set_axis_position(T_axis_id id, double position, bool apply);
  E_pmac_errno apply_axis_position(T_cs_id id);

  // set/get position offset
  E_pmac_errno set_axis_pos_offset(T_axis_id id, double offset);
  E_pmac_errno get_axis_pos_offset(T_axis_id id, double& offset);

  // set/get compensation table state
  E_pmac_errno set_axis_compensation(T_axis_id id, bool enabled);
  E_pmac_errno get_axis_compensation(T_axis_id id, bool& enabled);
  E_pmac_errno get_cs_compens_valid(T_cs_id id, bool& valid);

  // set/get cs velocity
  E_pmac_errno set_cs_velocity(T_cs_id id, double velocity);
  E_pmac_errno get_cs_velocity(T_cs_id id, double& velocity);

  // set/get cs acceleration
  E_pmac_errno set_cs_acceleration(T_cs_id id, double acceleration);
  E_pmac_errno get_cs_acceleration(T_cs_id id, double& acceleration);

  // set/get cs deceleration
  E_pmac_errno set_cs_deceleration(T_cs_id id, double deceleration);
  E_pmac_errno get_cs_deceleration(T_cs_id id, double& deceleration);

  // set/get cs s-curve
  E_pmac_errno get_cs_scurve(T_cs_id id, double& scurve);
  E_pmac_errno set_cs_scurve(T_cs_id id, double scurve);

  // get list of axis positions
  E_pmac_errno get_axes_positions(std::vector<T_axis_id> ids, std::vector<double>& pos_list);

  // set/get cs moving mode
  E_pmac_errno set_cs_moving_mode(T_cs_id id, E_moving_mode_t mode);
  E_pmac_errno get_cs_moving_mode(T_cs_id id, E_moving_mode_t& mode);

  // Gets the last error as a string.
  // @return Error string
  std::string get_error_string();

  // motion programs management
  E_pmac_errno start_motion_program(T_cs_id id, int mp);
  E_pmac_errno get_motion_program_state(T_cs_id id, E_mp_state_t& st);
  E_pmac_errno pause_current_motion_program(T_cs_id id);
  E_pmac_errno resume_current_motion_program(T_cs_id id);

private:

  // the command below are extracted from Observatory Science PmacControl library:
  /**
   * Remove trailing delimiters from the string and returns it.
   * param s - String to be trimmed.
   * param delimiters - List of delimiters to be removed from the string. 
   * If it is not specified, the default value is "\r\n" is used..
   * return Trimmed string
   */    
  inline std::string trim_right_copy(
                  const std::string& s, const std::string& delimiters = "\r\n")
  {
      size_t t = s.find_last_not_of( delimiters );
      if (t != std::string::npos)
          return s.substr(0, t + 1);
      else
          return s.substr(0, 0);
  }

  /**
   * @brief Checks if a string has a "error #". 
   * 
   * This function can be used to check a reply string from the Power PMAC if it reports any error. 
   * 
   * @param s - The string to be checked.
   * @return Error number found in the string. If no error is found in the string, 0.
   */
  inline int check_PowerPMAC_error(const std::string s)
  {
    std::string error_str("error #");
    size_t index = s.find(error_str);
    
    if (index == std::string::npos)
    {
      return 0; // No error
    }
    
    // get error number
    std::string s2 = s.substr(index + error_str.length());
    size_t index2 = s2.find(":"); // get index of ':'
    
    if (index2 == std::string::npos)
    {
      return 0; // No error
    }
    
    std::string numstring = s2.substr(0, index2);
    int error_number;
    std::istringstream(numstring) >> error_number;

    return error_number;    
  }

  // analyze motor status: if bit set to 1, write it in returned string
  std::string analyze_motor_status(T_motor_status raw_status);

#if !defined (_SIMULATION_)

  // write command and read reply on ssh connection
  E_pmac_errno write_read(std::string cmd, std::string& reply);

  // CPU usage computation function
  E_pmac_errno compute_cpu_usage_info(double& phase_usage, double& servo_usage, 
    double& rti_usage, double& bg_usage);

#endif

  // connection state
  bool m_is_connected;

  // Last error string
  std::string m_error_string;

#if defined (_SIMULATION_)  
  // Box status
  T_ppmac_brick_status m_sim_box_status;

  // CS list
  std::map<T_cs_id, CsParams> m_sim_cs_param;

  // Axes parameters
  std::map<T_axis_id, AxisParams> m_sim_axis_params;

  // Axes current state
  std::map<T_axis_id, AxisState> m_sim_axis_state;

  // test
  bool m_sim_mp_state;
#else

  // ssh driver access
  SSHDriver * m_sshdriver;

  // Check if CS is declared in axis list
  bool is_cs_declared(T_cs_id id);

  // memorization of axis position
  AxisPos_t m_axis_list;

  // memorization of CPU usage values
  double m_last_positive_servoTaskTime;
  double m_last_positive_rtIntTaskTime;

#endif


  };
} // namespace sgonpmaclib

#endif // _POWER_PMAC_H_
