//=============================================================================
// PMACTypesAndConsts.h
//=============================================================================
// abstraction.......Basic types and Constants for SMARGON PMAC library
// class.............Basic structures
// original author...S. MINOLLI - NEXEYA
//=============================================================================

#ifndef _PMAC_TYPES_AND_CONSTS_H_
#define _PMAC_TYPES_AND_CONSTS_H_

//=============================================================================
// DEPENDENCIES
//=============================================================================
#include <vector>
#include <string>
#include <map>
#include <iostream>

#include <SGonPMAC/SGonPMACTypes.h>

namespace sgonpmaclib
{

  //---------------------------------------------------------------------------
  //- Types for DeltaTAU commands and R/W access to attributes
  //---------------------------------------------------------------------------

  //- Generic string
  const std::string kPMAC_VAR_NAME_CMD_SEP = "].";
  const std::string kPMAC_CONNECT_GPASCII = "gpascii -2\n";
  const std::string kPMAC_VAR_NAME_STATUS = "Status";

  // Available CS values:
  const T_cs_id kSMARGON_CS_VALUE = 1;

  // Available axis names:
  const std::string kPMAC_VAR_NAME_AXIS_A = "A";
  const std::string kPMAC_VAR_NAME_AXIS_B = "B";
  const std::string kPMAC_VAR_NAME_AXIS_C = "C";
  const std::string kPMAC_VAR_NAME_AXIS_X = "X";
  const std::string kPMAC_VAR_NAME_AXIS_Y = "Y";
  const std::string kPMAC_VAR_NAME_AXIS_Z = "Z";

  //- PMAC BOX attributes:
  const std::string kPMAC_VAR_NAME_BOX_STATUS_ABORT = "sys.AbortAll\n";
  const std::string kPMAC_VAR_NAME_BOX_STATUS_OV = "BrickLV.BusOverVoltage\n";
  const std::string kPMAC_VAR_NAME_BOX_STATUS_OT = "BrickLV.OverTemp\n";
  const std::string kPMAC_VAR_NAME_BOX_STATUS_BUV = "BrickLV.BusUnderVoltage\n";

  const std::string kPMAC_VAR_NAME_BOX_FWD_KIN_ERR = "fwdKinematicError\n";
  const std::string kPMAC_VAR_NAME_BOX_INV_KIN_ERR = "invKinematicError\n";
  const std::string kPMAC_VAR_NAME_BOX_CALC_COMP_ERR = "calcCompError\n";

  const std::string kPMAC_VAR_NAME_CS_STATUS = "Sys.Status\n";
  const std::string kPMAC_CPU_TEMP = "Sys.CpuTemp\n";
  const std::string kPMAC_RUN_TIME = "Sys.Time\n";
  const std::string kPMAC_PHASE_TIME = "Sys.FltrPhaseTime\n";
  const std::string kPMAC_SERVO_TIME = "Sys.FltrServoTime\n";
  const std::string kPMAC_RT_TIME = "Sys.FltrRtIntTime\n";
  const std::string kPMAC_BG_TIME = "Sys.FltrBgTime\n";

  const std::string kPMAC_VAR_NAME_VERS = "vers\n";
  const std::string kPMAC_VAR_NAME_VERS_MAJ = "versionMajor\n";
  const std::string kPMAC_VAR_NAME_VERS_MIN = "versionMinor\n";
  const std::string kPMAC_VAR_NAME_VERS_SUB = "versionSub\n";

  //- PMAC Box commands:
  const std::string kPMAC_CMD_NAME_REBOOT = "reboot\n";
  const std::string kPMAC_CMD_NAME_ABORT = "&* abort\n";
  const std::string kPMAC_CMD_NAME_RESET = "$$$\n";

  //- PMAC CS attributes:
  const std::string kPMAC_VAR_NAME_GLOBAL_STATUS = "&1?\n";
  const std::string kPMAC_VAR_NAME_CS_ABORT = "abort\n";
  const std::string kPMAC_VAR_NAME_CS_INIT_REF = "enable plc referencing\n";
  const std::string kPMAC_VAR_NAME_CS_REF_STATUS = "referencingStatus\n";
  const std::string kPMAC_VAR_NAME_CS_SETPOS = "cpx abs frax(A,B,C,X,Y,Z)";
  const std::string kPMAC_VAR_NAME_CS_COORD = "Coord[";

  const std::string kPMAC_VAR_NAME_CS_SET_VEL = "Tm";
  const std::string kPMAC_VAR_NAME_CS_SET_ACCEL = "Ta";
  const std::string kPMAC_VAR_NAME_CS_SET_DECEL = "Td";
  const std::string kPMAC_VAR_NAME_CS_SET_SCURVE = "Ts";
  const std::string kPMAC_VAR_NAME_CS_MP_ACTIVE = "ProgActive";
  const std::string kPMAC_VAR_NAME_CS_MP_RUNNING = "ProgProceeding";


  //- PMAC AXIS attributes:
  const std::string kPMAC_VAR_NAME_AX_SETPOS = "cpx abs";
  const std::string kPMAC_VAR_NAME_AX_OFFSET = "StubOffset";

  const std::string kPMAC_VAR_NAME_AX_COMPENS_1 = "compensation";
  const std::string kPMAC_VAR_NAME_AX_COMPENS_2 = "Enabled";
  const std::string kPMAC_VAR_NAME_AX_COMPENS_ST = "compensationDataValid\n";

  // Motor attributes:
  const std::string kPMAC_VAR_NAME_MOTOR = "Motor[";

  // Motor commands:
  const std::string kPMAC_CMD_NAME_ON = "&1 enable #1..6j/\n";
  const std::string kPMAC_CMD_NAME_OFF_BASE = "&1 disable plc referencing";
  const std::string kPMAC_CMD_NAME_MOTOR_ALL = "#1..6kill\n";
  const std::string kPMAC_CMD_NAME_MOTOR_OMEGA = "#6kill\n";
  const std::string kPMAC_CMD_NAME_MOTOR_OTHER = "#1..5kill\n";


#if defined (_SIMULATION_)
  //-------------------------------------------------------------------------
  //- CS and axis definition
  //-------------------------------------------------------------------------
  //- CS parameters FOR SIMULATION ONLY
  typedef struct CsParams
  {
    // status
    T_cs_status status;

    // referencing status
    E_cs_referencing_status_t ref_status;

    // velocity
    double velocity;

    // acceleration time
    double accel_time;

    // deceleration time
    double decel_time;

    // scurve time
    double scurve_time;

    // moving mode
    E_moving_mode_t mv_mode;

    // compensation data valid
    bool compens_valid;

    //- default constructor --
    CsParams ()
      : ref_status(CS_REF_POS_UNKNOWN),
        velocity(0.0),
        accel_time(0.0),
        decel_time(0.0),
        scurve_time(0.0),
        mv_mode(CS_MV_MODE_LINEAR),
        compens_valid(true)
    {
    }

    //- destructor -----------
    ~CsParams ()
    {
    }

    //- copy constructor -----
    CsParams (const CsParams& src)
    {
      *this = src;
    }

    //- operator= --------------
    const CsParams & operator= (const CsParams& src)
    {
        if (this == & src) 
          return *this;

        this->scurve_time = src.scurve_time;
        this->status = src.status;
        this->ref_status = src.ref_status;
        this->velocity = src.velocity;
        this->accel_time = src.accel_time;
        this->decel_time = src.decel_time;
        this->mv_mode = src.mv_mode;
        this->compens_valid = src.compens_valid;

        return *this;
    }

    //- dump -------------------
    void dump () const
    {
      std::cout << "CsParams::velocity........." 
                << this->velocity
                << std::endl; 
      std::cout << "CsParams::acceleration time........." 
                << this->accel_time
                << std::endl; 
      std::cout << "CsParams::deceleration time........." 
                << this->decel_time
                << std::endl; 
      std::cout << "CsParams::scurve_time........." 
                << this->scurve_time
                << std::endl;
      std::cout << "CsParams::mv_mode........." 
                << this->mv_mode
                << std::endl; 
      std::cout << "CsParams::compens_valid........." 
                << this->compens_valid
                << std::endl; 
      std::cout << "CsParams::ref_status........." 
                << this->ref_status
                << std::endl; 
    }
  } CsParams;


  //- Axis parameters FOR SIMULATION ONLY
  typedef struct AxisParams
  {
    //- members ------------

    // compensation table
    bool compens;

    // offset
    double offset;

    //- default constructor --
    AxisParams ()
      : compens(false),
        offset(0.0)
    {
    }

    //- destructor -----------
    ~AxisParams ()
    {
    }

    //- copy constructor -----
    AxisParams (const AxisParams& src)
    {
      *this = src;
    }

    //- operator= --------------
    const AxisParams & operator= (const AxisParams& src)
    {
        if (this == & src) 
          return *this;

        this->offset = src.offset;
        this->compens = src.compens;

        return *this;
    }

    //- dump -------------------
    void dump () const
    {
      std::cout << "AxisParams::compens........." 
                << this->compens
                << std::endl; 
      std::cout << "AxisParams::offset........." 
                << this->offset
                << std::endl; 
    }
  } AxisParams;

  //- Axis current state FOR SIMULATION ONLY
  typedef struct AxisState
  {
    //- members ------------
    // current position
    double position;

    // position setpoint
    double pos_setpoint;

    //- default constructor --
    AxisState ()
      : position(0.0),
        pos_setpoint(0.0)
    {
    }

    //- destructor -----------
    ~AxisState ()
    {
    }

    //- copy constructor -----
    AxisState (const AxisState& src)
    {
      *this = src;
    }

    //- operator= --------------
    const AxisState & operator= (const AxisState& src)
    {
        if (this == & src) 
          return *this;

        this->pos_setpoint = src.pos_setpoint;
        this->position = src.position;

        return *this;
    }

    //- dump -------------------
    void dump () const
    {
      std::cout << "AxisParams::pos_setpoint........." 
                << this->pos_setpoint
                << std::endl; 
      std::cout << "AxisParams::position........." 
                << this->position
                << std::endl; 
    }
  } AxisState;
#endif


  //--------------------------------------------------------------------------
  //- PMAC library interface
  //--------------------------------------------------------------------------
  
  //- PMAC library error numbers
  enum E_pmac_errno {
    // No error occurred
    pmac_NO_ERROR = 0,
    pmac_configuration_ERROR, // configuration error (e.g. bad axis name, IP address)
    pmac_libssh_ERROR,        // SSH library error (e.g. communication error)
    pmac_internal_ERROR,      // SGON lib internal error (e.g. null pointer)
    pmac_range_ERROR,         // Value out of range (e.g. negative valocity)
    pmac_CTRL_ERROR           // PMAC controller error (in a command reply)
  };


  //- (Cs, axis name) <-> memorized position mapping
  typedef std::map<T_axis_id, double> AxisPos_t;
  typedef AxisPos_t::iterator AxisPos_it_t;


  //--------------------------------------------------------------------------
  //- Box status bits
  //--------------------------------------------------------------------------

#define Bit0                 0x0000000000000001
#define Bit1                 0x0000000000000002
#define Bit2                 0x0000000000000004
#define Bit3                 0x0000000000000008
#define Bit4                 0x0000000000000010
#define Bit5                 0x0000000000000020
#define Bit6                 0x0000000000000040
#define Bit7                 0x0000000000000080
#define Bit8                 0x0000000000000100
#define Bit9                 0x0000000000000200
#define Bit10                0x0000000000000400
#define Bit11                0x0000000000000800
#define Bit12                0x0000000000001000
#define Bit13                0x0000000000002000
#define Bit14                0x0000000000004000
#define Bit15                0x0000000000008000
#define Bit16                0x0000000000010000
#define Bit17                0x0000000000020000
#define Bit18                0x0000000000040000
#define Bit19                0x0000000000080000
#define Bit20                0x0000000000100000
#define Bit21                0x0000000000200000
#define Bit22                0x0000000000400000
#define Bit23                0x0000000000800000
#define Bit24                0x0000000001000000
#define Bit25                0x0000000002000000
#define Bit26                0x0000000004000000
#define Bit27                0x0000000008000000
#define Bit28                0x0000000010000000
#define Bit29                0x0000000020000000
#define Bit30                0x0000000040000000
#define Bit31                0x0000000080000000

  // sys.status bits

#define SYS_STATUS_BGWDTFault      Bit0
#define SYS_STATUS_RtIntWDTFault   Bit1
#define SYS_STATUS_PwrOnFault      Bit2
#define SYS_STATUS_ProjectLoadErr  Bit3
#define SYS_STATUS_ConfigLoadErr   Bit4
#define SYS_STATUS_HWChangeErr     Bit5
#define SYS_STATUS_FileConfigErr   Bit6
#define SYS_STATUS_Default         Bit7
#define SYS_STATUS_NoClocks        Bit8

#define BOX_STATUS_NoERROR         0
#define BOX_STATUS_BGWDTFault      Bit0
#define BOX_STATUS_RtIntWDTFault   Bit1
#define BOX_STATUS_PwrOnFault      Bit2
#define BOX_STATUS_ProjectLoadErr  Bit3
#define BOX_STATUS_ConfigLoadErr   Bit4
#define BOX_STATUS_HWChangeErr     Bit5
#define BOX_STATUS_FileConfigErr   Bit6
#define BOX_STATUS_Default         Bit7
#define BOX_STATUS_NoClocks        Bit8
#define BOX_STATUS_AbortAll        Bit9
#define BOX_STATUS_FwdKinErr       Bit13
#define BOX_STATUS_InvKinErr       Bit14
#define BOX_STATUS_CalcCompErr     Bit15
#define BOX_STATUS_BusOverVoltage  Bit16
#define BOX_STATUS_BoardOverTemp   Bit17
#define BOX_STATUS_BusUnderVoltage Bit18


//- Binary word for motor status
struct T_motor_status {
  unsigned int NotUsed0 : 1; // reserved
  unsigned int NotUsed1 : 1; // reserved
  unsigned int NotUsed2 : 1; // reserved
  unsigned int NotUsed3 : 1; // reserved
  unsigned int SpindleMotor0 : 1; // Spindle axis definition status (bit 0)
  unsigned int SpindleMotor1 : 1; // Spindle axis definition status (bit 1)
  unsigned int GantryHomed : 1; // Gantry homing complete
  unsigned int TriggerSpeedSel : 1; // Triggered move speed selected

  unsigned int PhaseFound : 1; // Phase reference established
  unsigned int BlockRequest : 1; // Block request flag set
  unsigned int NotUsed4 : 1; // reserved
  unsigned int InPos : 1; // In position
  unsigned int AmpEna : 1; // Amplifier enabled
  unsigned int ClosedLoop : 1; // Closed-loop mode
  unsigned int DesVelZero : 1; // Desired velocity zero
  unsigned int HomeComplete : 1; // Home complete

  unsigned int NotUsed5 : 1; // Move timer enabled
  unsigned int NotUsed6 : 1; // reserved
  unsigned int EncLoss : 1; // Sensor loss error
  unsigned int AmpWarn : 1; // Amp warning
  unsigned int TriggerNotFound : 1; // Trigger not found
  unsigned int I2tFault : 1; // Integrated current (I2T) fault
  unsigned int SoftPlusLimit : 1; // Software positive limit set
  unsigned int SoftMinusLimit : 1; // Software negative limit set

  unsigned int AmpFault : 1; // Amplifier fault
  unsigned int LimitStop : 1; // Stopped on hardware limit
  unsigned int FeFatal : 1; // Fatal following error
  unsigned int FeWarn : 1; // Warning following error
  unsigned int PlusLimit : 1; // Hardware positive limit set
  unsigned int MinusLimit : 1; // Hardware negative limit set
  unsigned int HomeInProgress : 1; // Home search move in progress
  unsigned int TriggerMove : 1; // Trigger search move in progress
};


  // Box connection timeout
  const int kPMAC_BOX_TIMEOUT = 3000; // ms

} //- namespace sgonpmaclib

#endif //- _PMAC_TYPES_AND_CONSTS_H_

